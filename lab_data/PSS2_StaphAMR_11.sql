/*
Short project name: StaphAMR
Requested by: Teresa Street
Work log no.: 11
Extracted by: Jack Cregan
Extract date: 03/04/2020
Server name: SOLO
DB version: PSS2_live
*/


-- micro susceptibility results

SELECT ClusterID			
		,CollectionDateTime			
		,AccessionNumber
		,BatTestCode
		,SpecimenCode
		,BugName
		,SusceptabilityMethod			
		,DrugCode			
		,DrugName			
		,DrugResult			
		,MIC
FROM [pss2_live].[micro]
WHERE (AccessionNumber =  'F40235' and convert(date, CollectionDateTime, 112) = '20181130')
		or (AccessionNumber =  'F45588' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45590' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45591' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45594' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45595' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45596' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F46990' and convert(date, CollectionDateTime, 112) = '20190621')
		or (AccessionNumber =  'F47404' and convert(date, CollectionDateTime, 112) = '20190705')
		or (AccessionNumber =  'F47895' and convert(date, CollectionDateTime, 112) = '20190719')
		or (AccessionNumber =  'F50035' and convert(date, CollectionDateTime, 112) = '20190913')
		or (AccessionNumber =  'F52801' and convert(date, CollectionDateTime, 112) = '20191129')
		or (AccessionNumber =  'H48507' and convert(date, CollectionDateTime, 112) = '20190221')
		or (AccessionNumber =  'M14548' and convert(date, CollectionDateTime, 112) = '20190107')
		or (AccessionNumber =  'M18588' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18589' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18590' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18703' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M19410' and convert(date, CollectionDateTime, 112) = '20190610')
		or (AccessionNumber =  'M26097' and convert(date, CollectionDateTime, 112) = '20200106')
		or (AccessionNumber =  'S79219' and convert(date, CollectionDateTime, 112) = '20190720')
		or (AccessionNumber =  'T13228' and convert(date, CollectionDateTime, 112) = '20191008')
		or (AccessionNumber =  'T8853' and convert(date, CollectionDateTime, 112) = '20190618')
		or (AccessionNumber =  'W51803' and convert(date, CollectionDateTime, 112) = '20181003')
		or (AccessionNumber =  'W56006' and convert(date, CollectionDateTime, 112) = '20190130')
		or (AccessionNumber =  'W61167' and convert(date, CollectionDateTime, 112) = '20190619')
		or (AccessionNumber =  'W61172' and convert(date, CollectionDateTime, 112) = '20190619')
		or (AccessionNumber =  'W64382' and convert(date, CollectionDateTime, 112) = '20190925')
		or (AccessionNumber =  'W64389' and convert(date, CollectionDateTime, 112) = '20190925')
		or (AccessionNumber =  'W65277' and convert(date, CollectionDateTime, 112) = '20191023')
		-- accession number known, collection date "the back end of last year"
		or (AccessionNumber = 'M25424' and CollectionDateTime BETWEEN '20191101' AND '20191231')

-- phoenix results
SELECT [CollectionDateTime]
      ,[AccessionNumber]
      ,[SpecimenType]
      ,[Organism]
      ,[Antibiotic]
      ,[MIC]
      ,[MICInterpretation]
      ,[ExpertRuleInterpretation]
      ,[FinalResult]
      ,[ExpertRuleNumber]
      ,[ExpertRuleDescription]
  FROM [pss2_live].[micro_phoenix]
  WHERE (AccessionNumber =  'F40235' and convert(date, CollectionDateTime, 112) = '20181130')
		or (AccessionNumber =  'F45588' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45590' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45591' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45594' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45595' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F45596' and convert(date, CollectionDateTime, 112) = '20190510')
		or (AccessionNumber =  'F46990' and convert(date, CollectionDateTime, 112) = '20190621')
		or (AccessionNumber =  'F47404' and convert(date, CollectionDateTime, 112) = '20190705')
		or (AccessionNumber =  'F47895' and convert(date, CollectionDateTime, 112) = '20190719')
		or (AccessionNumber =  'F50035' and convert(date, CollectionDateTime, 112) = '20190913')
		or (AccessionNumber =  'F52801' and convert(date, CollectionDateTime, 112) = '20191129')
		or (AccessionNumber =  'H48507' and convert(date, CollectionDateTime, 112) = '20190221')
		or (AccessionNumber =  'M14548' and convert(date, CollectionDateTime, 112) = '20190107')
		or (AccessionNumber =  'M18588' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18589' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18590' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M18703' and convert(date, CollectionDateTime, 112) = '20190513')
		or (AccessionNumber =  'M19410' and convert(date, CollectionDateTime, 112) = '20190610')
		or (AccessionNumber =  'M26097' and convert(date, CollectionDateTime, 112) = '20200106')
		or (AccessionNumber =  'S79219' and convert(date, CollectionDateTime, 112) = '20190720')
		or (AccessionNumber =  'T13228' and convert(date, CollectionDateTime, 112) = '20191008')
		or (AccessionNumber =  'T8853' and convert(date, CollectionDateTime, 112) = '20190618')
		or (AccessionNumber =  'W51803' and convert(date, CollectionDateTime, 112) = '20181003')
		or (AccessionNumber =  'W56006' and convert(date, CollectionDateTime, 112) = '20190130')
		or (AccessionNumber =  'W61167' and convert(date, CollectionDateTime, 112) = '20190619')
		or (AccessionNumber =  'W61172' and convert(date, CollectionDateTime, 112) = '20190619')
		or (AccessionNumber =  'W64382' and convert(date, CollectionDateTime, 112) = '20190925')
		or (AccessionNumber =  'W64389' and convert(date, CollectionDateTime, 112) = '20190925')
		or (AccessionNumber =  'W65277' and convert(date, CollectionDateTime, 112) = '20191023')
		-- accession number known, collection date "the back end of last year"
		or (AccessionNumber = 'M25424' and CollectionDateTime BETWEEN '20191101' AND '20191231')
