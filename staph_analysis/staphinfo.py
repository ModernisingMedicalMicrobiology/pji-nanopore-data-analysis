import pandas as pd
import numpy as np
from ast import literal_eval

meta=pd.read_excel('../lab_data/PJI sequencing culture summary.xlsx')
#meta=meta[meta['treatment']=='saponin 5%']
meta=meta[['sample_ID',
    'patient',
    'sample_type']]

meta.dropna(subset=['patient'],inplace=True)
#meta['patient']=meta['patient'].map(int)
metaD={}
for i,r in meta.iterrows():
    metaD.setdefault(r['patient'],{}).setdefault(r['sample_type'],[]).append(r['sample_ID'])


df=pd.read_csv('../speciation/stats.csv')
def fillList(r):
    if pd.isna(r['Detected species'])==True:
        return '[]'
    else:
        return r['Detected species']#.map(literal_eval)

df['species list']=df.apply(fillList,axis=1)
df['True species']=df['True species'].map(literal_eval)
df['species list']=df['species list'].map(literal_eval)
df['False species']=df['False species'].map(literal_eval)

def isitstaph(r):
    staph=False
    for i in r['True species']:
        if i.startswith('Staphyl'):
            staph=True
    for i in r['False species']:
        if i.startswith('Staphyl'):
            staph=True
    if type(r['species list'])==list:
        for i in r['species list']:
            if i.startswith('Staphyl'):
                staph=True
    return staph

def isitAuerus(r):
    staph=False
    for i in r['True species']:
        if i.endswith('aureus'):
            staph=True
    for i in r['False species']:
        if i.endswith('aureus'):
            staph=True
    if type(r['species list'])==list:
        for i in r['species list']:
            if i.endswith('aureus'):
                staph=True
    return staph

df['Found staph']=df.apply(isitstaph,axis=1)
df['Found aureus']=df.apply(isitAuerus,axis=1)
staphs=df[df['Found staph']== True]
staphs=staphs[staphs['Found aureus']== True]
#print(staphs)

################## mapping results ###############################################
def removeRedo(r):
    return r.replace('redo_','').replace('PJI_','').replace('carpae_','')

maps=pd.read_csv('../crumpit_out/all_map.csv')
maps['run_ID']=maps['sample'].map(removeRedo)

########## add caprae reruns that include caprae as a species ####################
cpruns=[('RPB004_2','barcode04'),
        ('RPB004_10','barcode02'),
        ('RPB004_54','barcode07'),
        ('RPB004_63','barcode11')]
cmaps=pd.read_csv('../crumpit_out/caprae_all_map.csv')
cmaps['run_ID']=cmaps['sample'].map(removeRedo)
cmaps=cmaps[cmaps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=maps[~maps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=pd.concat([maps,cmaps]) # add new runs
maps['chrom max']=maps.groupby(['run_ID','barcode','Species name'])['len'].transform(max)
maps['chrom longest']=np.where(maps['len']==maps['chrom max'], True, False)
maps=maps[maps['chrom longest']==True]
maps['10xCovBreadth']=(maps['x10']/maps['chrom max'])*100


ts=[item for sublist in staphs['True species'].to_list() for item in sublist]
fs=[item for sublist in staphs['False species'].to_list() for item in sublist]
staphsspecies=ts
staphsspecies.extend(fs)
maps=maps[maps['Species name'].isin(staphsspecies)]

sa=maps[maps['Species name']=='Staphylococcus aureus']


### merge dfs
df=staphs.merge(sa,on=['run_ID','barcode'],how='left')
#print(df)

## load micro/phoenix data
bugs={'STAPHYLOCOCCUS AUREUS':'Staphylococcus aureus',
       'PSEUDOMONAS AERUGINOSA':'Pseudomonas aeruginosa',
        'ENTEROBACTER CLOACAE':'Enterobacter cloacae',
        'METHICILLIN RESISTANT STAPHYLOCOCCUS AUREUS (MRSA) ISOLATED':'Staphylococcus aureus (MRSA)',
        'STAPHYLOCOCCUS EPIDERMIDIS(CoNS)':'Staphylococcus epidermidis'}

def renameBug(r):
    if r in bugs:
        return bugs[r]
    else: return r


micro=pd.read_csv('../lab_data/PSS2_StaphAMR_11_Micro.csv')
phoenix=pd.read_csv('../lab_data/PSS2_StaphAMR_11_Phoenix_nick.csv')
microAdd=pd.read_csv('../lab_data/PSS2_StaphAMR_11_20200713_AdditionalMicroSusceptibilities.csv')
phoenixAdd=pd.read_csv('../lab_data/PSS2_StaphAMR_11_20200713_AdditionalPhoenix.csv')
phoenixAdd=phoenixAdd[['AccessionNumber', 'Organism','Antibiotic','MIC','FinalResult']]
accD={'F46986':'F46990','T13229':'T13228'}
def mapD(r):
    if r in accD:
        return accD[r]
    else:
        return r
phoenixAdd['AccessionNumber']=phoenixAdd['AccessionNumber'].map(mapD)
microAdd['AccessionNumber']=microAdd['AccessionNumber'].map(mapD)
micro=pd.concat([micro,microAdd])
phoenix=pd.concat([phoenix,phoenixAdd])

micro['Organism']=micro['BugName'].map(renameBug)
micro=micro[micro['Organism'].isin(['Staphylococcus aureus','Staphylococcus aureus (MRSA)'])]
micro.rename(columns={'DrugResult':'Micro ABX',
    'MIC':'Micro MIC'},inplace=True)

micro=micro.pivot_table(index=['AccessionNumber','Organism'],
        columns='DrugName',
        values=['Micro ABX','Micro MIC'],
        aggfunc=lambda x: ' '.join(x))


phoenix.rename(columns={'FinalResult':'Phoenix ABX',
    'MIC':'Phoenix MIC'},inplace=True)
phoenix=phoenix[phoenix['Organism'].isin(['Staphylococcus aureus','Staphylococcus aureus (MRSA)'])]
phoenix2=phoenix.pivot_table( index=['AccessionNumber','Organism'],
        columns='Antibiotic',
        values=['Phoenix ABX','Phoenix MIC'], 
         aggfunc=lambda y: ' '.join(map(str, y)) )


AMR=micro.merge(phoenix2,on=['AccessionNumber','Organism'],how='outer')
AMRDF=meta.merge(AMR, left_on='sample_ID', right_on='AccessionNumber', how='right')
df=df.merge(AMR, left_on=['sample_ID'],right_on=['AccessionNumber'],how='outer')

### load gene coverage
def getRunBar(r):
    s=r['sample name'].replace('PJI_','').split('_')
    barcode=s[-1]
    run='RPB004_{0}'.format(s[1])
    r['run_ID']=run
    r['barcode']=barcode
    return r

chromCov=pd.read_csv('generickBugWorkflow_out/geneCovs.csv')
chromCov.rename(columns={'Unnamed: 0':'gene'},inplace=True)
chromCov=chromCov.apply(getRunBar,axis=1)
chromCov=chromCov[chromCov['chrom']=='BX571856.1']
chromCov=chromCov[['run_ID', 'barcode', 'gene','mean coverage' ]]
chromCov['gene']=chromCov['gene']+' X cov'
cc2=chromCov.pivot_table(index=['run_ID', 'barcode'], columns=['gene'], values='mean coverage')
df=df.merge(cc2,on=['run_ID','barcode'],how='left')

## load mobile genes
mobile=pd.read_csv('generickBugWorkflow_out/mobile_geneCovs.csv')
mobile=mobile[~mobile['chrom'].isin(['blaZ','mecA'])]
mobile=mobile.apply(getRunBar,axis=1)
mobile.rename(columns={'chrom':'mobile gene'},inplace=True)
mobile['mobile gene']=mobile['mobile gene'] + ' % cov'
mobile['covBreadth1x']=mobile['covBreadth1x']*100

mobile=mobile[['run_ID', 'barcode', 'mobile gene','covBreadth1x' ]]
mm2=mobile.pivot_table(index=['run_ID', 'barcode'], columns=['mobile gene'], values='covBreadth1x')
df=df.merge(mm2,on=['run_ID','barcode'],how='left')

## load SNPs
def getMut(r):
    mut='{0}{1}{2}'.format(r['WT res'],r['AA pos'],r['res'])
    return mut

muts=pd.read_csv('generickBugWorkflow_out/checkMuts.csv')
muts=muts.apply(getRunBar,axis=1)
muts['mutation']=muts.apply(getMut, axis=1)
muts=muts[muts['mut type']!='Wild type']
muts=muts[['run_ID', 'barcode', 'gene','mutation']]
muts['gene']=muts['gene']+ ' mutations'
muts.drop_duplicates(inplace=True)
muts = muts.groupby(['run_ID', 'barcode', 'gene'])['mutation'].apply(list).reset_index(name='mutations')
muts=muts.pivot_table(index=['run_ID', 'barcode'], columns=['gene'], values='mutations',aggfunc=lambda y: ' '.join(map(str, y)))
#print(muts)
df=df.merge(muts,on=['run_ID','barcode'],how='left')

###### predict resistance ########

def predictCirpo(r):
    cipro=[]
    SNPs=r['gyrA mutations']
    if type(SNPs) == list:
        gSNPs=['S84L', 'E88K', 'G106D', 'S85P', 'E88G', 'E88L']
        if any(x in SNPs for x in gSNPs) and r['gyrA X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['gyrA X cov'] >= 10:
            cipro.append('S')
        elif r['gyrA X cov'] < 10:
            cipro.append('U')
    SNPs=r['grlA mutations']
    if type(SNPs) == str:
        gSNPs=['S80F', 'S80Y', 'E84K', 'E84G', 'E84V', 'D432G', 'Y83N', 'A116E', 'I45M', 'A48T', 'D79V', 'V41G', 'S108N']
        if any(x in SNPs for x in gSNPs) and r['grlA X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['grlA X cov'] >= 10:
            cipro.append('S')
        elif r['grlA X cov'] < 10:
            cipro.append('U')

    SNPs=r['grlB mutations']
    if type(SNPs) == list:
        gSNPs=['R470D*', 'E422D*', 'P451S*', 'P585S*', 'D443E*', 'R444S*']
        if any(x in SNPs for x in gSNPs) and r['grlB X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['grlB X cov'] >= 10:
            cipro.append('S')
        elif r['grlA X cov'] < 10:
            cipro.append('U')
    if 'R' in cipro:
        return 'R'
    elif 'U' in cipro:
        return 'U'
    elif r['grlB X cov'] >= 10 and r['gyrA X cov'] >= 10 and r['grlA X cov'] >= 10:
        return 'S'
    else: return 'U'

def predictClindo(r):
    genes=['ermA', 'ermB', 'ermC', 'ermT']
    results=[]
    for gene in genes:
        col='{0} % cov'.format(gene)
        if col in r:
            if r[col]>98:
                results.append('R')

    if 'R' in results and r['gyrA X cov']>=10:
        return 'R'
    if 'R' not in results and r['gyrA X cov']>=10:
        return 'S'
    else: return 'U'

mobilesDeterminates=pd.read_csv('catalogs_staph_catalog_Staph_mobile_determinants.tsv',sep='\t')
abxMobile=mobilesDeterminates.groupby(['Antimicrobial agent(s)'])['Gene'].apply(list).to_dict()
chromDeterminants=pd.read_csv('catalogs_staph_catalog_Staph_chromosome_determinants.tsv',sep='\t')

abxChrom=chromDeterminants.groupby(['Antimicrobial agent','Gene'])['Amino acid substitutions'].apply(list).to_dict()

def predictPresent(r,abx='Erythromycin and clindamycin'):
    genes=abxMobile[abx]
    results=[]
    for gene in genes:
        col='{0} % cov'.format(gene)
        col2='{0} X cov'.format(gene)
        if col in r:
            if r[col]>98:
                results.append('R')
        elif col2 in r:
            if r[col2]>10:
                results.append('R')

    if 'R' in results and r['10xCovBreadth']>=80:
        return 'R'
    if 'R' not in results and r['10xCovBreadth']>=80:
        return 'S'
    else: return 'U'

def predictChrom(r, abx='Ciprofloxacin',gene='fusA'):
    results=[]
    SNPs=r['{0} mutations'.format(gene)]
    gSNPs=chromDeterminants.loc[(chromDeterminants['Antimicrobial agent']==abx) & (chromDeterminants['Gene']==gene),['Amino acid substitutions']].values[0][0]
    gSNPs=gSNPs.split(', ')
    if type(SNPs) == str:
        SNPs=literal_eval(SNPs)
        #print(abx, gene, SNPs, r['{0} X cov'.format(gene)], gSNPs)
        if any(x in SNPs for x in gSNPs) and r['{0} X cov'.format(gene)] >= 10:
            results.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['{0} X cov'.format(gene)] >= 10:
            results.append('S')
        elif r['{0} X cov'.format(gene)] < 10:
            results.append('U')
    if 'R' in results:
        return 'R'
    elif 'U' in results:
        return 'U'
    elif r['{0} X cov'.format(gene)] >= 10:
        return 'S'
    else: return 'U'

df['Nano Ciprofloxacin']=df.apply(predictCirpo, axis=1)
df['Nano Clindamycin']=df.apply(predictClindo, axis=1)
df['Nano Erythromycin and clindamycin']=df.apply(predictPresent, axis=1, abx='Erythromycin and clindamycin')

for abx in abxMobile:
    df['Nano {0}'.format(abx)]=df.apply(predictPresent, axis=1, abx=abx)

for abx,gene in abxChrom:
    #print(abx, gene)
    df['Nano {0} {1}'.format(abx, gene)]=df.apply(predictChrom, axis=1, abx=abx, gene=gene)


addCols=['blaZ X cov','dfrB X cov',
    'fusA X cov','grlA X cov',
    'grlB X cov', 'gyrA X cov',	
    'mecA X cov','rpoB X cov',
    'aacA-aphD % cov',
    'ermA % cov', 'ermB % cov',
    'ermC % cov', 'ermT % cov', 
    'far % cov','fusB % cov', 'fusC % cov',
    'tetK % cov',
    'tetM % cov', 'vanA % cov', 
    'dfrB mutations', 'fusA mutations', 
    'grlA mutations', 'grlB mutations', 
    'gyrA mutations', 'rpoB mutations']
for c in addCols:
    if c not in df.columns:
        df[c]=None

df.to_csv('staphs.csv',index=False)
df2=df[['patient','sample_ID','run_ID', 'barcode','covBread_1x_percent', 'avCov',
    ('Micro ABX', 'CIPROFLOXACIN'), ('Phoenix ABX', 'Ciprofloxacin'),'Nano Ciprofloxacin','Nano Ciprofloxacin grlA','Nano Ciprofloxacin grlB','Nano Ciprofloxacin gyrA',
    ('Micro ABX', 'CLINDAMYCIN'), ('Phoenix ABX', 'Clindamycin'),'Nano Erythromycin and clindamycin',
    ('Micro ABX', 'ERYTHROMYCIN'),  ('Phoenix ABX', 'Erythromycin'),'Nano Erythromycin and clindamycin',
    ('Micro ABX', 'FUSIDIC ACID'), ('Phoenix ABX', 'Fusidic Acid'),'Nano Fusidic acid', 'Nano Fusidic acid fusA',
    ('Micro ABX', 'FLUCLOXACILLIN'),('Micro ABX', 'CEFOXITIN'),('Phoenix ABX', 'Cefoxitin'),'Nano Methicillin',
    ('Micro ABX', 'MUPIROCIN'), ('Phoenix ABX', 'Mupirocin'),'Nano Mupirocin (high-level resistance)',
    ('Micro ABX', 'GENTAMICIN'), ('Phoenix ABX', 'Gentamicin'),'Nano Gentamicin',
    ('Micro ABX', 'PEN/AMOXICILLIN'), ('Micro ABX', 'PENICILLIN'), ('Micro ABX', 'PENICILLIN G'), ('Phoenix ABX', 'Penicillin G'),'Nano Penicillin',
    ('Micro ABX', 'RIFAMPICIN'), ('Phoenix ABX', 'Rifampin'),'Nano Rifampin rpoB',
    ('Micro ABX', 'TETRACYCLINE'), ('Phoenix ABX', 'Tetracycline'),'Nano Tetracycline',
    ('Micro ABX', 'TRIMETHOPRIM'),('Phoenix ABX', 'Trimethoprim'),'Nano Trimethoprim','Nano Trimethoprim dfrB',
    ('Micro ABX', 'VANCOMYCIN'),('Phoenix ABX', 'Vancomycin'),'Nano Vancomycin',
    'blaZ X cov','dfrB X cov',
    'fusA X cov','grlA X cov',
    'grlB X cov', 'gyrA X cov',	
    'mecA X cov','rpoB X cov', 
    'aacA-aphD % cov',
    'ermA % cov', 'ermB % cov',
    'ermC % cov', 'ermT % cov', 
    'far % cov','fusB % cov', 'fusC % cov',
    'tetK % cov',
    'tetM % cov', 'vanA % cov', 
    'dfrB mutations', 'fusA mutations', 
    'grlA mutations', 'grlB mutations', 
    'gyrA mutations', 'rpoB mutations']]


cols=df.columns
cols=[col for col in cols if type(col) == tuple]
cold={}
for col in cols:
    cold[col]='{0} {1}'.format(col[0][0],col[-1])
df2.rename(columns=cold, inplace=True)
AMRDF.rename(columns=cold, inplace=True)
AMRDF.to_csv('AMRDF.csv')
df2.to_csv('staphs_renamed.csv',index=False)
df3=df2.dropna(subset=['sample_ID','run_ID', 'barcode'])
#df2.columns = df2.columns.str.replace("Phoenix ABX", 'P')
df3.to_csv('staphs_reduced.csv',index=False)

### make smaller summary table
abxList=[{'Lab results':['M CIPROFLOXACIN','P Ciprofloxacin'],
        'Sequencing results':['Nano Ciprofloxacin','Nano Ciprofloxacin grlA','Nano Ciprofloxacin grlB','Nano Ciprofloxacin gyrA'],
        'abx':'Cipro'},
        {'Lab results':['M CLINDAMYCIN','P Clindamycin'],
            'Sequencing results':['Nano Erythromycin and clindamycin','M ERYTHROMYCIN','P Erythromycin', 'Nano Erythromycin and clindamycin'],
            'abx':'Clind/Eryth'},
        {'Lab results':['M FUSIDIC ACID','P Fusidic Acid'],
            'Sequencing results':['Nano Fusidic acid','Nano Fusidic acid fusA'],
            'abx':'Fus'},
        {'Lab results':['M FLUCLOXACILLIN','M CEFOXITIN','P Cefoxitin'],
            'Sequencing results':['Nano Methicillin'],
            'abx':'Meth'},
        {'Lab results':['M MUPIROCIN','P Mupirocin'],
            'Sequencing results':['Nano Mupirocin (high-level resistance)'],
            'abx':'Mup'},
        {'Lab results':['M GENTAMICIN','P Gentamicin'],
            'Sequencing results':['Nano Gentamicin'],
            'abx':'Gent'},
        {'Lab results':['M PEN/AMOXICILLIN','M PENICILLIN','M PENICILLIN G','P Penicillin G'],
            'Sequencing results':['Nano Penicillin'],
            'abx':'Pen'},
        {'Lab results':['M RIFAMPICIN',	'P Rifampin'], 
            'Sequencing results':['Nano Rifampin rpoB'],
            'abx':'rif'}, 
        {'Lab results':['M TETRACYCLINE', 'P Tetracycline'],
            'Sequencing results':['Nano Tetracycline'],
            'abx':'tet'}, 
        {'Lab results':['M TRIMETHOPRIM','P Trimethoprim'],
            'Sequencing results':['Nano Trimethoprim','Nano Trimethoprim dfrB'],
            'abx':'trim'},
        {'Lab results':['M VANCOMYCIN',	'P Vancomycin'],
            'Sequencing results':['Nano Vancomycin'],
            'abx':'van'}]

options={}
def checkABX(abx,r):
     lab=r[abx['Lab results']]
     results=[]
     for n,i in enumerate(lab):
         s=i.split(' ')
         results.extend(s)
         # count numbers of each type
         if i in options:
             options[i]+=1
         else:
             options[i]=1
     return results

def checkTissue(abx,r):
    d=metaD[r['patient']]
    tissueResults=[]
    if 'synovial fluid' in d:
        for a in d['synovial fluid']:
            r2=AMRDF[AMRDF['sample_ID']==a]
            lab=r2[abx['Lab results']]
            for n,i in enumerate(lab.values[0]):
                try:
                    s=i.split(' ')
                    tissueResults.extend(s) 
                except:
                    pass
    elif 'tissue' in d:
        print(abx,d)
        for a in d['tissue']:
            r2=AMRDF[AMRDF['sample_ID']==a]
            lab=r2[abx['Lab results']]
            for n,i in enumerate(lab.values[0]):
                try:
                    s=i.split(' ')
                    tissueResults.extend(s)
                except:
                    pass

    if 'R' in tissueResults:
        return 'R(t)'
    elif any(i in ['S','SS'] for i in tissueResults) and 'R' not in tissueResults:
        return 'S(t)'
    elif all(i not in ['S','SS'] for i in tissueResults) and 'R' not in tissueResults:
        return 'U'


def reduceABX(r):
    for abx in abxList:
        ## lab results
        lab=checkABX(abx,r)
        if 'R' in lab:
            r['Lab {0}'.format(abx['abx'])]='R'
        elif any(i in ['S','SS'] for i in lab) and 'R' not in lab:
            r['Lab {0}'.format(abx['abx'])]='S'
        elif all(i not in ['S','SS'] for i in lab) and 'R' not in lab:
            v=checkTissue(abx,r)
            r['Lab {0}'.format(abx['abx'])]=v

        ## sequencing results
        if 'R' in ''.join(r[abx['Sequencing results']]):
            r['Seq {0}'.format(abx['abx'])]='R'
        elif all([i=='N' for i in r[abx['Sequencing results']]]):
            r['Seq {0}'.format(abx['abx'])]='N'
        elif 'U' in ''.join(r[abx['Sequencing results']]):
            r['Seq {0}'.format(abx['abx'])]='U'
        else:
            r['Seq {0}'.format(abx['abx'])]='S'
    return r

df3.fillna('N',inplace=True)
df3=df3.apply(reduceABX, axis=1)
print(options)

# remove sample 2 and 5
df3=df3[~df3['patient'].isin(['2','53'])]
df3.to_csv('staphs_reduced_summary.csv',index=False)

def errors(r):
    Concordant=0
    major_error=0
    very_major_error=0
    sf=0
    lf=0
    for abx in abxList:
        a=abx['abx']
        s=r['Seq {}'.format(a)]
        l=r['Lab {}'.format(a)]
        if s in ['R','S'] and l in ['S','R']:
            if s==l:
                Concordant+=1
            elif l == 'S' and s == 'R':
                major_error+=1
            elif l == 'R' and s == 'S':
                very_major_error+=1
        else:
            if s == 'U':
                sf+=1
            if l == 'N':
                lf+=1
    r['Concordant']=Concordant
    r['major error']=major_error
    r['very major error']=very_major_error
    r['Lab no result']=lf
    r['sequencing insufficient depth']=sf
    return r

df3=df3.apply(errors,axis=1)
c=sum(df3['Concordant'])
me=sum(df3['major error'])
vme=sum(df3['very major error'])
meR=(me/c)*100
vmeR=(vme/c)*100
u=sum(df3['sequencing insufficient depth'])
lf=sum(df3['Lab no result'])
print('Concordant',c)
print('major error',me, meR)
print('very major error',vme, vmeR)
print('sequencing insufficient depth',u)
print('No lab result',lf)


###### all ABX columns
#    ('Micro ABX', 'CHLORAMPHENICOL'),	('Micro ABX', 'CIPROFLOXACIN'),	('Micro ABX', 'CLINDAMYCIN'),
#    ('Micro ABX', 'DAPTOMYCIN'), ('Micro ABX', 'ERYTHROMYCIN'),
#    ('Micro ABX', 'FLUCLOXACILLIN'), ('Micro ABX', 'FOSFOMYCIN'),
#    ('Micro ABX', 'FUSIDIC ACID'), ('Micro ABX', 'GENTAMICIN'),
#    ('Micro ABX', 'LEVOFLOXACIN'), ('Micro ABX', 'LINEZOLID'),
#    ('Micro ABX', 'MOXIFLOXACIN'), ('Micro ABX', 'MUPIROCIN'), ('Micro ABX', 'PEN/AMOXICILLIN'),
#    ('Micro ABX', 'PENICILLIN'), ('Micro ABX', 'PENICILLIN G'), ('Micro ABX', 'RIFAMPICIN'), ('Micro ABX', 'SYNERCID'),
#    ('Micro ABX', 'TEICOPLANIN'), ('Micro ABX', 'TETRACYCLINE'), ('Micro ABX', 'TRIMETH-SULFAMETHOXA'),
#    ('Micro ABX', 'TRIMETHOPRIM'), ('Micro ABX', 'VANCOMYCIN'), 
#    ('Phoenix ABX', 'Ampicillin'),('Phoenix ABX', 'Cefoxitin'),	('Phoenix ABX', 'Ceftaroline'),
#    ('Phoenix ABX', 'Chloramphenicol'), ('Phoenix ABX', 'Ciprofloxacin'), ('Phoenix ABX', 'Clindamycin'),
#    ('Phoenix ABX', 'Daptomycin'),('Phoenix ABX', 'Erythromycin'), ('Phoenix ABX', 'Fosfomycin w/G6P'),
#    ('Phoenix ABX', 'Fusidic Acid'), ('Phoenix ABX', 'Gentamicin'), ('Phoenix ABX', 'Imipenem'), 
#    ('Phoenix ABX', 'Levofloxacin'), ('Phoenix ABX', 'Linezolid'), ('Phoenix ABX', 'Moxalactam'),
#    ('Phoenix ABX', 'Moxifloxacin'), ('Phoenix ABX', 'Mupirocin'), ('Phoenix ABX', 'Mupirocin High level'),
#    ('Phoenix ABX', 'Oxacillin'), ('Phoenix ABX', 'Penicillin G'),('Phoenix ABX', 'Quinupristin-dalfopristin'),
#    ('Phoenix ABX', 'Rifampin'), ('Phoenix ABX', 'Teicoplanin'), ('Phoenix ABX', 'Tetracycline'),
#    ('Phoenix ABX', 'Tigecycline'), ('Phoenix ABX', 'Tobramycin'), ('Phoenix ABX', 'Trimethoprim'), 
#    ('Phoenix ABX', 'Trimethoprim-Sulfamethoxazole'), ('Phoenix ABX', 'Vancomycin'),
