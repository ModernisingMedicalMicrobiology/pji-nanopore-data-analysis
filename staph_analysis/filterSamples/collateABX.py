import pandas as pd
import numpy as np
from ast import literal_eval


# load genome depth
df=pd.read_csv('generickBugWorkflow_out/genomeDepth.csv')
df['sample name']=df['run name']
df=df.fillna(0)
df['10xCovBreadth']=df['covBreadth10x'].map(int)

chromCov=pd.read_csv('generickBugWorkflow_out/geneCovs.csv')
chromCov.rename(columns={'Unnamed: 0':'gene'},inplace=True)
chromCov=chromCov[chromCov['chrom']=='BX571856.1']
chromCov=chromCov[['sample name', 'gene','mean coverage' ]]
chromCov['gene']=chromCov['gene']+' X cov'
cc2=chromCov.pivot_table(index=['sample name'], columns=['gene'], values='mean coverage')
df=df.merge(cc2,on=['sample name'],how='left')

## load mobile genes
mobile=pd.read_csv('generickBugWorkflow_out/mobile_geneCovs.csv')
mobile=mobile[~mobile['chrom'].isin(['blaZ','mecA'])]
mobile.rename(columns={'chrom':'mobile gene'},inplace=True)
mobile['mobile gene']=mobile['mobile gene'] + ' % cov'
mobile['covBreadth1x']=mobile['covBreadth1x']*100

mobile['sample name']=mobile['run name']
mobile=mobile[['sample name', 'mobile gene','covBreadth1x' ]]
mm2=mobile.pivot_table(index=['sample name'], columns=['mobile gene'], values='covBreadth1x')
df=df.merge(mm2,on=['sample name'],how='left')

## load SNPs
def getMut(r):
    mut='{0}{1}{2}'.format(r['WT res'],r['AA pos'],r['res'])
    return mut

muts=pd.read_csv('generickBugWorkflow_out/checkMuts.csv')
muts['mutation']=muts.apply(getMut, axis=1)
muts=muts[muts['mut type']!='Wild type']
muts=muts[['sample name', 'gene','mutation']]
muts['gene']=muts['gene']+ ' mutations'
muts.drop_duplicates(inplace=True)
muts = muts.groupby(['sample name', 'gene'])['mutation'].apply(list).reset_index(name='mutations')
muts=muts.pivot_table(index=['sample name'], columns=['gene'], values='mutations',aggfunc=lambda y: ' '.join(map(str, y)))
df=df.merge(muts,on=['sample name'],how='left')
###### predict resistance ########

def predictCirpo(r):
    cipro=[]
    SNPs=r['gyrA mutations']
    if type(SNPs) == list:
        gSNPs=['S84L', 'E88K', 'G106D', 'S85P', 'E88G', 'E88L']
        if any(x in SNPs for x in gSNPs) and r['gyrA X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['gyrA X cov'] >= 10:
            cipro.append('S')
        elif r['gyrA X cov'] < 10:
            cipro.append('U')
    SNPs=r['grlA mutations']
    if type(SNPs) == str:
        gSNPs=['S80F', 'S80Y', 'E84K', 'E84G', 'E84V', 'D432G', 'Y83N', 'A116E', 'I45M', 'A48T', 'D79V', 'V41G', 'S108N']
        if any(x in SNPs for x in gSNPs) and r['grlA X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['grlA X cov'] >= 10:
            cipro.append('S')
        elif r['grlA X cov'] < 10:
            cipro.append('U')

    SNPs=r['grlB mutations']
    if type(SNPs) == list:
        gSNPs=['R470D*', 'E422D*', 'P451S*', 'P585S*', 'D443E*', 'R444S*']
        if any(x in SNPs for x in gSNPs) and r['grlB X cov'] >= 10:
            cipro.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['grlB X cov'] >= 10:
            cipro.append('S')
        elif r['grlA X cov'] < 10:
            cipro.append('U')
    if 'R' in cipro:
        return 'R'
    elif 'U' in cipro:
        return 'U'
    elif r['grlB X cov'] >= 10 and r['gyrA X cov'] >= 10 and r['grlA X cov'] >= 10:
        return 'S'
    else: return 'U'

def predictClindo(r):
    genes=['ermA', 'ermB', 'ermC', 'ermT']
    results=[]
    for gene in genes:
        col='{0} % cov'.format(gene)
        if col in r:
            if r[col]>98:
                results.append('R')

    if 'R' in results and r['gyrA X cov']>=10:
        return 'R'
    if 'R' not in results and r['gyrA X cov']>=10:
        return 'S'
    else: return 'U'

mobilesDeterminates=pd.read_csv('../catalogs_staph_catalog_Staph_mobile_determinants.tsv',sep='\t')
abxMobile=mobilesDeterminates.groupby(['Antimicrobial agent(s)'])['Gene'].apply(list).to_dict()
chromDeterminants=pd.read_csv('../catalogs_staph_catalog_Staph_chromosome_determinants.tsv',sep='\t')

abxChrom=chromDeterminants.groupby(['Antimicrobial agent','Gene'])['Amino acid substitutions'].apply(list).to_dict()

def predictPresent(r,abx='Erythromycin and clindamycin'):
    genes=abxMobile[abx]
    results=[]
    for gene in genes:
        col='{0} % cov'.format(gene)
        col2='{0} X cov'.format(gene)
        if col in r:
            if r[col]>98:
                results.append('R')
        elif col2 in r:
            if r[col2]>10:
                results.append('R')

    if 'R' in results and r['10xCovBreadth']>=80:
        return 'R'
    if 'R' not in results and r['10xCovBreadth']>=80:
        return 'S'
    else: return 'U'

def predictChrom(r, abx='Ciprofloxacin',gene='fusA'):
    results=[]
    SNPs=r['{0} mutations'.format(gene)]
    gSNPs=chromDeterminants.loc[(chromDeterminants['Antimicrobial agent']==abx) & (chromDeterminants['Gene']==gene),['Amino acid substitutions']].values[0][0]
    gSNPs=gSNPs.split(', ')
    if type(SNPs) == str:
        SNPs=literal_eval(SNPs)
        if any(x in SNPs for x in gSNPs) and r['{0} X cov'.format(gene)] >= 10:
            results.append('R')
        elif all(x not in SNPs for x in gSNPs) and r['{0} X cov'.format(gene)] >= 10:
            results.append('S')
        elif r['{0} X cov'.format(gene)] < 10:
            results.append('U')
    if 'R' in results:
        return 'R'
    elif 'U' in results:
        return 'U'
    elif r['{0} X cov'.format(gene)] >= 10:
        return 'S'
    else: return 'U'

df['Nano Ciprofloxacin']=df.apply(predictCirpo, axis=1)
df['Nano Clindamycin']=df.apply(predictClindo, axis=1)
df['Nano Erythromycin and clindamycin']=df.apply(predictPresent, axis=1, abx='Erythromycin and clindamycin')

for abx in abxMobile:
    df['Nano {0}'.format(abx)]=df.apply(predictPresent, axis=1, abx=abx)

for abx,gene in abxChrom:
    df['Nano {0} {1}'.format(abx, gene)]=df.apply(predictChrom, axis=1, abx=abx, gene=gene)

addCols=['blaZ X cov','dfrB X cov',
    'fusA X cov','grlA X cov',
    'grlB X cov', 'gyrA X cov',
    'mecA X cov','rpoB X cov',
    'aacA-aphD % cov',
    'ermA % cov', 'ermB % cov',
    'ermC % cov', 'ermT % cov',
    'far % cov','fusB % cov', 'fusC % cov',
    'tetK % cov',
    'tetM % cov', 'vanA % cov',
    'dfrB mutations', 'fusA mutations',
    'grlA mutations', 'grlB mutations',
    'gyrA mutations', 'rpoB mutations']
for c in addCols:
    if c not in df.columns:
        df[c]=None

df.to_csv('staphs.csv',index=False)

df3=df[['sample name','covBreadth1x', 'avDepth',
    'blaZ X cov','dfrB X cov',
    'fusA X cov','grlA X cov',
    'grlB X cov', 'gyrA X cov',
    'mecA X cov','rpoB X cov',
    'aacA-aphD % cov',
    'ermA % cov', 'ermB % cov',
    'ermC % cov', 'ermT % cov',
    'far % cov','fusB % cov', 'fusC % cov',
    'tetK % cov',
    'tetM % cov', 'vanA % cov',
    'dfrB mutations', 'fusA mutations',
    'grlA mutations', 'grlB mutations',
    'gyrA mutations', 'rpoB mutations']]



############## 
abxList=[{'Lab results':['M CIPROFLOXACIN','P Ciprofloxacin'],
        'Sequencing results':['Nano Ciprofloxacin','Nano Ciprofloxacin grlA','Nano Ciprofloxacin grlB','Nano Ciprofloxacin gyrA'],
        'abx':'Cipro'},
        {'Lab results':['M CLINDAMYCIN','P Clindamycin'],
            'Sequencing results':['Nano Erythromycin and clindamycin','Nano Erythromycin and clindamycin'],
            'abx':'Clind/Eryth'},
        {'Lab results':['M FUSIDIC ACID','P Fusidic Acid'],
            'Sequencing results':['Nano Fusidic acid','Nano Fusidic acid fusA'],
            'abx':'Fus'},
        {'Lab results':['M FLUCLOXACILLIN','M CEFOXITIN','P Cefoxitin'],
            'Sequencing results':['Nano Methicillin'],
            'abx':'Meth'},
        {'Lab results':['M MUPIROCIN','P Mupirocin'],
            'Sequencing results':['Nano Mupirocin (high-level resistance)'],
            'abx':'Mup'},
        {'Lab results':['M GENTAMICIN','P Gentamicin'],
            'Sequencing results':['Nano Gentamicin'],
            'abx':'Gent'},
        {'Lab results':['M PEN/AMOXICILLIN','M PENICILLIN','M PENICILLIN G','P Penicillin G'],
            'Sequencing results':['Nano Penicillin'],
            'abx':'Pen'},
        {'Lab results':['M RIFAMPICIN', 'P Rifampin'],
            'Sequencing results':['Nano Rifampin rpoB'],
            'abx':'rif'},
        {'Lab results':['M TETRACYCLINE', 'P Tetracycline'],
            'Sequencing results':['Nano Tetracycline'],
            'abx':'tet'},
        {'Lab results':['M TRIMETHOPRIM','P Trimethoprim'],
            'Sequencing results':['Nano Trimethoprim','Nano Trimethoprim dfrB'],
            'abx':'trim'},
        {'Lab results':['M VANCOMYCIN', 'P Vancomycin'],
            'Sequencing results':['Nano Vancomycin'],
            'abx':'van'}]

def checkABX(abx,r):
     lab=r[abx['Lab results']]
     results=[]
     for n,i in enumerate(lab):
         s=i.split(' ')
         results.extend(s)
         # count numbers of each type
         if i in options:
             options[i]+=1
         else:
             options[i]=1
     return results

def checkTissue(abx,r):
    d=metaD[r['patient']]
    tissueResults=[]
    if 'synovial fluid' in d:
        for a in d['synovial fluid']:
            r2=AMRDF[AMRDF['sample_ID']==a]
            lab=r2[abx['Lab results']]
            for n,i in enumerate(lab.values[0]):
                try:
                    s=i.split(' ')
                    tissueResults.extend(s)
                except:
                    pass
    elif 'tissue' in d:
        for a in d['tissue']:
            r2=AMRDF[AMRDF['sample_ID']==a]
            lab=r2[abx['Lab results']]
            for n,i in enumerate(lab.values[0]):
                try:
                    s=i.split(' ')
                    tissueResults.extend(s)
                except:
                    pass

    if 'R' in tissueResults:
        return 'R(t)'
    elif any(i in ['S','SS'] for i in tissueResults) and 'R' not in tissueResults:
        return 'S(t)'
    elif all(i not in ['S','SS'] for i in tissueResults) and 'R' not in tissueResults:
        return 'U'

def reduceABX(r):
    for abx in abxList:
        ## sequencing results
        if 'R' in ''.join(r[abx['Sequencing results']]):
            r['Seq {0}'.format(abx['abx'])]='R'
        elif all([i=='N' for i in r[abx['Sequencing results']]]):
            r['Seq {0}'.format(abx['abx'])]='N'
        elif 'U' in ''.join(r[abx['Sequencing results']]):
            r['Seq {0}'.format(abx['abx'])]='U'
        else:
            r['Seq {0}'.format(abx['abx'])]='S'
    return r

df=df.apply(reduceABX, axis=1)
df.to_csv('staphs_reduced_summary.csv',index=False)

