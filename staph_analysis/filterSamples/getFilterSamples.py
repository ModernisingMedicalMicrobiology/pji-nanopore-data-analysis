# coding: utf-8
from scipy.stats import binom
rv = binom(65, 50)
rv
get_ipython().run_line_magic('ls', '')
get_ipython().run_line_magic('pwd', '')
import pandas as pd
meta=pd.read_excel('/Users/nick/soft/pji-nanopore-data-analysis/lab_data/PJI sequencing culture summary.xlsx')
meta
meta=meta[meta['treatment']=='filter 5uM']
meta
staph=pd.read_csv('/Users/nick/soft/pji-nanopore-data-analysis/staph_analysis/staphs_reduced.csv')
staph
staph=pd.read_csv('/Users/nick/soft/pji-nanopore-data-analysis/staph_analysis/Staph_saponin_effect.csv')
staph
samples=staph['sample_ID'].to_list()
samples
meta=meta[meta['sample_ID'].isin(samples)]
meta
meta.columns()
meta.columns
meta[['sample_ID','run_ID', 'barcode']]
meta[['patient','sample_ID','run_ID', 'barcode']]
meta=meta[['patient','sample_ID','run_ID', 'barcode']]
meta
meta['barcodeFull']='barcode' + meta['barcode'].map(str)
meta
meta['barcodeFull']='barcode' + meta['barcode'].apply(lambda x: x.zfill(1))
meta['barcodeFull']='barcode' + meta['barcode'].map(str).apply(lambda x: x.zfill(1))
meta
meta['barcodeFull']='barcode' + meta['barcode'].map(str).apply(lambda x: x.zfill(2))
meta
meta['id']=meta['run_ID'] + '_' + meta['barcodeFull']
meta
meta.to_csv('filter_staphs.csv')
meta.to_csv('filter_staphs.csv',index=False)
get_ipython().system('rsync -avP filter_staphs.csv sandy:/mnt/Data2/staph_nano_muts/filterSamples/')
meta['fastq']='/mnt/nanostore/ana/nick/PJI/ana/'+ meta['run_ID'] + '/basecalled_fastq/' + meta['barcodeFull'] + '*'
meta
meta['patient']=meta['patient'].map(int)
meta
meta2=meta[['patient','fastq','run_ID']]
meta2
meta2.to_csv('runFiles.csv',index=False)
get_ipython().system('rsync -avP runFiles.csv sandy:/mnt/Data2/staph_nano_muts/filterSamples/')
get_ipython().run_line_magic('save', 'current_session ~0/')
