#!/usr/bin/env python3
import pandas as pd
import numpy as np
from ast import literal_eval
import seaborn as sns
import matplotlib.pyplot as plt

df=pd.read_csv('staphs.csv')
meta=pd.read_excel('../lab_data/PJI sequencing culture summary.xlsx')

samples=df['sample_ID'].to_list()
meta=meta[meta['sample_ID'].isin(samples)]
meta=meta[meta['treatment'].isin(['filter 5uM','saponin 5%'])]
meta=meta[['sample_ID','run_ID','barcode','treatment']]
meta.sort_values(by=['sample_ID'],inplace=True)
meta['barcode']='barcode' + meta['barcode'].map(lambda x: f'{x:0>2}')

################## mapping results ###############################################
def removeRedo(r):
    return r.replace('redo_','').replace('PJI_','').replace('carpae_','')

maps=pd.read_csv('../crumpit_out/all_map.csv')
maps['run_ID']=maps['sample'].map(removeRedo)
maps=maps[maps['Species name']=='Staphylococcus aureus']

df=meta.merge(maps,how='left')
print(df)

# saponin effect
df2=df[['sample_ID','treatment','covBread_1x_percent','avCov']]
sap=df2[df2['treatment']=='saponin 5%']
fil=df2[df2['treatment']=='filter 5uM']

df3=sap.merge(fil,on='sample_ID', how='left')

rd={'covBread_1x_percent_x':'Saponin coverage breadth',
        'covBread_1x_percent_y':'Filter coverage breadth',
        'avCov_x':'Saponin average coverage',
        'avCov_y':'Filter average coverage'}

df3=df3.rename(columns=rd)
df3=df3[['sample_ID', 'Saponin coverage breadth', 'Saponin average coverage',
        'Filter coverage breadth','Filter average coverage']]

df3['Fold improvement']=df3['Saponin average coverage']/df3['Filter average coverage']
df3['% improvement']=(df3['Filter average coverage']/df3['Saponin average coverage'])*100
print(df3)
df3.to_csv('Staph_saponin_effect.csv')

## plotting
p=sns.swarmplot(x='treatment',y='avCov',data=df)#, hue='sample_ID')
plt.savefig('Staph_sap_avdepth.pdf')
plt.show()

