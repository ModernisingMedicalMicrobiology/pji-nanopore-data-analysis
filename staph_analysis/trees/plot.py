from ete3 import PhyloTree, TreeStyle, ProfileFace, Tree, TextFace, random_color,SVG_COLORS
import pandas as pd
import numpy as np
import json
from random import randrange
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
plt.rcParams['svg.fonttype'] = 'none'


df=pd.read_csv('meta.csv')
df=pd.read_excel('../../lab_data/PJI sequencing culture summary.xlsx')
def getNode(r):
    run=r['run_ID'].split('_')[-1]
    if r['barcode']=='12a':
        barcode=r['barcode']
    else:
        barcode="{:02d}".format(int(r['barcode']))
    r['leaf']='R{0}B{1}'.format(run,barcode)
    r['run number']=str(run)
    if r['barcode']=='12a':
        r['barcode']=12
    return r

df=df.apply(getNode,axis=1)
# load tree
t=Tree(open('output_tree/cluster_ml/cluster_1_cf_scaled.tree','rt').read())

## reduce to only samples in tree
leaves=[]
for lf in t.iter_leaves():
    leaf=lf.get_leaf_names(is_leaf_fn=None)[0]
    leaves.append(leaf)
df=df[df['leaf'].isin(leaves)]
df['patient']=df['patient'].map(int)
df['patient']=df['patient'].map(str)
df['Staph culture']=np.where(df['patient']!='53',True, False)

## remove tips with other treatments
keep_list = df[(df['treatment'].isin(['saponin 5%','filter 5uM']) ) & (df['sample_type']=='sonication fluid') ]['leaf'].to_list()
#keep_list = df[ df['treatment'].isin(['saponin 5%','filter 5uM']) ]['leaf'].to_list()
t.prune(keep_list)


leaves=[]
for lf in t.iter_leaves():
    leaf=lf.get_leaf_names(is_leaf_fn=None)[0]
    leaves.append(leaf)

### get SNPs
snps=pd.read_csv('output_tree/align-compare.txt',sep='\t',names=['leaf','leaf comp','SNPs'])
snps2=pd.read_csv('output_tree/align-compare.txt',sep='\t',names=['leaf comp','leaf','SNPs'])
snps2=snps2[['leaf','leaf comp','SNPs']]
snps=pd.concat([snps,snps2])
snps=snps[snps['leaf'].isin(leaves)]
snps=snps[snps['leaf comp'].isin(leaves)]
g=snps.groupby('leaf')['SNPs'].min()
g=g.reset_index()
df=df.merge(g,how='left')
#df['SNPs']=df['snps'].map(int)

# rename filter to correct uppercase M
def rname(r):
    return r.replace('uM','\u03BCm')
df['treatment']=df['treatment'].map(rname)


## colours
runCol,sampleCol,treatCol={},{},{}
# remove black
svg=list(SVG_COLORS)
svg.remove('black')

#for i in df['run number'].unique():
#    runCol[i]=svg[randrange(len(svg))]
#
#for i in df['patient'].unique():
#    sampleCol[i]=svg[randrange(len(svg))]
#
#colours={'runCols':runCol, 'sampleCols':sampleCol}
#with open('colours.json', 'w') as fp:
#    json.dump(colours, fp)

with open('colours.json', 'r') as fp:
    colours = json.load(fp)
    sampleCol=colours['sampleCols']
    runCol=colours['runCols']

#for i in df['treatment'].unique():
#    treatCol[i]=svg[randrange(len(svg))]

treatCol={'saponin 5%':'Khaki',
        'filter 5\u03BCm': 'DarkKhaki'}

staph_pos_cols={True:'Green',
        False:'Red'}

def SNP_cols(n):
    if n==0:
        return 'White'
    elif n < 10:
        return 'Yellow'
    elif n < 50:
        return 'Orange'
    elif n < 200:
        return 'OrangeRed'
    elif n < 1000:
        return 'Red'
    elif n > 1000:
        return 'Maroon'
    else:
        return 'Dark grey'
        

def mylayout(node):
    node.img_style["size"]=0
    node.img_style["vt_line_width"] = 2
    node.img_style["hz_line_width"] = 2

def addCell(text,color):
    cell=TextFace(text)
    cell.background.color=color

    cell.margin_top = 5 
    cell.margin_right = 5
    cell.margin_left = 5
    cell.margin_bottom = 5
    cell.opacity = 0.5 # from 0 to 1
#    cell.inner_border.width = 0 # 1 pixel border
#    cell.inner_border = 0  # dashed line
    cell.border.width = 1
    return cell

for lf in t.iter_leaves():
    leaf=lf.get_leaf_names(is_leaf_fn=None)[0]
    d=df[['patient','treatment','run number','barcode','Staph culture','SNPs']][df['leaf']==leaf].to_dict(orient='records')
#    print(leaf,d)
    
    # run number
    cell=addCell(d[0]['run number'],
            runCol[d[0]['run number']])
    lf.add_face(cell, column=0, position="aligned")
    # sample number
    cell=addCell(d[0]['patient'],
            sampleCol[d[0]['patient']])
    lf.add_face(cell, column=1, position="aligned")
    # sample number
    cell=addCell(d[0]['treatment'],
            treatCol[d[0]['treatment']])
    lf.add_face(cell, column=2, position="aligned")
    # staph +ve 
    cell=addCell(d[0]['Staph culture'],
            staph_pos_cols[d[0]['Staph culture']])
    lf.add_face(cell, column=3, position="aligned")
    # Closest neighbour SNP number  
    cell=addCell(d[0]['SNPs'],
            SNP_cols(d[0]['SNPs']) )
    lf.add_face(cell, column=4, position="aligned")

df.to_csv('used_meta.csv',index=False)

ts = TreeStyle()
ts.show_leaf_name = False
ts.layout_fn = mylayout
#t.render('Staph_trees_all.pdf',tree_style=ts)
t.render('Staph_trees.pdf',tree_style=ts)
t.render('Staph_trees.png',tree_style=ts,dpi=300)
t.render('Staph_trees.svg',tree_style=ts)

