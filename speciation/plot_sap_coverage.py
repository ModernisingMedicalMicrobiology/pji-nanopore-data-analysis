#!/usr/bin/env python3
import sys
from functools import partial
from multiprocessing import Pool
from ete3 import NCBITaxa
ncbi = NCBITaxa()
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

df=pd.read_csv('results/stats_cov45_propbases0.6_propmapped0.4_None_None.csv')
sap=pd.read_csv('saponin_reduction_5.csv')

samples=sap['sample_ID'].to_list()

dat=pd.read_csv('dat.csv')
dat=dat[dat['sample_ID_y'].isin(samples)]

dat['chroms']=dat.groupby(['run_ID','Barcode','treatment'])['covBread_1x_percent'].transform('count')
dat['chrom max']=dat.groupby(['run_ID','Barcode','treatment'])['covBread_1x_percent'].transform(max)
dat['chrom longest']=np.where(dat['covBread_1x_percent']==dat['chrom max'], True, False)
dat=dat[dat['chrom longest']==True]

dat['% genome covered at 10x depth']=(dat['x10']/dat['len'])*100
dat=dat[['sample_ID_y','treatment','covBread_1x_percent','% genome covered at 10x depth','avCov']]

df=df.merge(dat, left_on=['sample_ID','treatment'], right_on=['sample_ID_y','treatment'], how='left')
df=df[df['treatment'].isin(['saponin 5%','filter 5uM'])]
df=df[~df['sample_ID_y'].isna()]

#df.to_csv('/tmp/dat.csv')

def addMu(r):
    return r.replace('u','\u03BC')

df['treatment']=df['treatment'].map(addMu)
df.rename(columns={'covBread_1x_percent':'% genome covered at 1x depth','avCov':'Fold depth coverage'}
        ,inplace=True)
ax=sns.boxplot(x='treatment', y='% genome covered at 1x depth', data=df)
ax=sns.swarmplot(x='treatment', y='% genome covered at 1x depth', data=df, color=".25")
plt.savefig('figs/saponin_coverage.svg')
plt.savefig('figs/saponin_coverage.png')
plt.savefig('figs/saponin_coverage.pdf')
#plt.show()
plt.clf()

df['Human depletion method']=df["treatment"]
ax=sns.boxplot(x='Human depletion method', y='% genome covered at 10x depth', data=df)
ax=sns.swarmplot(x='Human depletion method', y='% genome covered at 10x depth', data=df, color=".25")
plt.ylabel('% bacterial genome covered at 10x depth')
plt.xlabel("Human depletion method")
plt.savefig('figs/saponin_10Xcoverage.svg')
plt.savefig('figs/saponin_10Xcoverage.png')
plt.savefig('figs/saponin_10Xcoverage.pdf')
#plt.show()
plt.clf()

#ax=sns.boxplot(x='treatment', y='avCov', data=df)
#ax=sns.swarmplot(x='treatment', y='avCov', data=df, color=".25")
#plt.savefig('figs/saponin_avCoverage.svg')
#plt.savefig('figs/saponin_avCoverage.png')
#plt.savefig('figs/saponin_avCoverage.pdf')
#plt.show()
#plt.clf()

ax=sns.scatterplot(x='% genome covered at 1x depth', y='Fold depth coverage', data=df, hue='Human depletion method')
#plt.ylabel('% bacterial genome covered at 1x depth')
plt.savefig('figs/saponin_depthCoverage.svg')
plt.savefig('figs/saponin_depthCoverage.png')
plt.savefig('figs/saponin_depthCoverage.pdf')
#plt.show()
plt.clf()

des=df.groupby('treatment')['% genome covered at 1x depth','% genome covered at 10x depth','Fold depth coverage'].describe()#.unstack(1)

print(des)
des.to_csv('/tmp/des.csv')
