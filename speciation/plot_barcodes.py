#!/usr/bin/env python3
import sys
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

def runNum(r):
    return int(r.replace('RPB004_',''))


domains=pd.read_csv('domains.csv')

def plotBarcode(df):
    df['True barcode']=np.where(pd.isna(df['sample_ID_y']), False, True)
    df['Total flowcell correct demultiplexing']=df.groupby(['run_ID','True barcode'])['total bases'].transform(sum)
    df['Total flowcell bases']=df.groupby(['run_ID'])['total bases'].transform(sum)
    df['Flowcell barcode assignment %']=(df['Total flowcell correct demultiplexing']/df['Total flowcell bases'])*100
    df['Run number']=df['run_ID'].map(runNum)
    df2=df[df['True barcode']==True]
    df2=df2[['Run number','Flowcell barcode assignment %']]
    df2.drop_duplicates(inplace=True)
    df2.sort_values(by='Run number',inplace=True,ascending=False)
    ax=df2.plot(kind='barh',x='Run number',legend=False)
    print(df2['Flowcell barcode assignment %'].describe())
    #df3=df['']
    plt.tight_layout()
    plt.xlabel("Flowcell barcode assignment %")
    #    plt.savefig('figs/total_run_bases.svg',format="svg")
    #    plt.savefig('figs/total_run_bases.png',format="png")
    #    plt.savefig('figs/total_run_bases.pdf',format="pdf")
    plt.show()

plotBarcode(domains)
