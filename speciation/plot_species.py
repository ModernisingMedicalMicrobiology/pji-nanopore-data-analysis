#!/usr/bin/env python3
import pandas as pd
import seaborn as sns
sns.set_style('darkgrid')
import matplotlib.pyplot as plt
from collections import Counter
from ast import literal_eval
from ete3 import NCBITaxa
ncbi = NCBITaxa()


df=pd.read_csv('results/stats_cov45_propbases0.6_propmapped0.4.csv')

df['True species']=df['True species'].map(literal_eval)
df['False negative species']=df['False negative species'].map(literal_eval)

FP=df['False species'].map(literal_eval)
TP=df['True species']
FN=df['False negative species']

TP=[i for s in TP for i in s]
FP=[i for s in FP for i in s]
FN=[i for s in FN for i in s]

c = Counter(TP)
dfTP=pd.DataFrame.from_dict(c,orient='index',columns=['True Positive'])
c = Counter(FP)
dfFP=pd.DataFrame.from_dict(c,orient='index',columns=['False Positive'])
c = Counter(FN)
dfFN=pd.DataFrame.from_dict(c,orient='index',columns=['False Negative'])



df=pd.concat([dfTP,dfFP,dfFN],axis=1,sort=True)
df=df.reset_index()
df.rename(columns={'index':'Species'},inplace=True)
df['Total']=df.sum(axis=1)

# Phylums
def getRank(r,taxon='phylum'):
    taxid=r['taxid'][0]
    lineage = ncbi.get_lineage(taxid)
    names = ncbi.get_taxid_translator(lineage)
    rank= ncbi.get_rank(lineage)
    ir={v: k for k, v in rank.items()}
    r[taxon]=names[ir[taxon]]
    return r


df=df[df['Species']!='Staphylococcus']
name2taxid = ncbi.get_name_translator(df['Species'].unique())
df['taxid']=df['Species'].map(name2taxid)
df=df.apply(getRank,axis=1)
df=df.apply(getRank,axis=1,taxon='genus')
df=df.apply(getRank,axis=1,taxon='family')
df['Phylum count']=df.groupby('phylum')['Species'].transform('count')

sorter=['Staphylococcaceae',
        'Streptococcaceae',
        'Enterococcaceae',
        'Enterobacteriaceae',
        'Peptoniphilaceae',
        'Debaryomycetaceae',
        'Corynebacteriaceae',
        'Propionibacteriaceae',
        'Fusobacteriaceae',
        'Prevotellaceae',
        'Morganellaceae',
        'Pseudomonadaceae',
        'Dermabacteraceae']
sorterIndex = dict(zip(sorter, range(len(sorter))))
df['sort_rank']=df['family'].map(sorterIndex)

print(df)

#df=df.sort_values(by='Total')
df=df.sort_values(by=['sort_rank','Total'],ascending=[False,True])
df[['Species','True Positive','False Positive','False Negative']].set_index('Species').plot(kind='barh', stacked=True)
plt.xlabel('Number of counts')
plt.tight_layout()
plt.savefig('figs/species_counts.pdf')
plt.savefig('figs/species_counts.svg')
plt.savefig('figs/species_counts.png',dpi=300)
#plt.show()
plt.clf()

df=df.groupby(['phylum'])['True Positive','False Positive','False Negative'].sum()
df=df.reset_index()
df['Total']=df.sum(axis=1)
df=df.sort_values(by='Total')
print(df)
df[['phylum','True Positive','False Positive','False Negative']].set_index('phylum').plot(kind='barh', stacked=True)
plt.xlabel('Number of counts')
plt.tight_layout()
plt.savefig('figs/phylum_counts.pdf')
plt.savefig('figs/phylum_counts.svg')
plt.savefig('figs/phylum_counts.png',dpi=300)
#plt.show()
