#!/usr/bin/env python3
import sys
from functools import partial
from multiprocessing import Pool
from ete3 import NCBITaxa
ncbi = NCBITaxa()
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.rcParams['svg.fonttype'] = 'none'
sns.set_style('darkgrid')



#############################  read in sample data (meta) before any sequencing info ###########


################### functions #####################################
def renameBarcode(r):
    return r.replace('BC','barcode')

def removeRedo(r):
    return r.replace('redo_','').replace('PJI_','').replace('carpae_','')

def sonicTaxons(r):
    sonicList=list(r[['sonication_culture_species1',
        'sonication_culture_species2',
        'sonication_culture_species3']])

    ignores=['negative',None]
    sonicList=[s for s in sonicList if s not in ignores]
    sonicList=[s for s in sonicList if not pd.isnull(s)]

    for species in sonicList:
        s=species.split(' ')
        if s[-1]=='sp':
            sonicList.append(s[0])
        if species=='coagulase negative Staphylococcus' or species=='Coagulase negative Staphylococcus':
            sonicList.append('Staphylococcus')
    name2taxid = ncbi.get_name_translator(sonicList)
    return name2taxid

def cultureTaxons(r):
    sonicList=list(r[['sonication_culture_species1',
        'sonication_culture_species2',
        'sonication_culture_species3',
        'tissue_culture_species1',
        'tissue_culture_species2',
        'tissue_culture_species3',
        'tissue_culture_species4',
        'tissue_culture_species5']])

    ignores=['negative',None]
    sonicList=[s for s in sonicList if s not in ignores]
    sonicList=[s for s in sonicList if not pd.isnull(s)]

    for species in sonicList:
        s=species.split(' ')
        if s[-1]=='sp':
            sonicList.append(s[0])
        if species=='coagulase negative Staphylococcus' or species=='Coagulase negative Staphylococcus':
            sonicList.append('Staphylococcus')
    name2taxid = ncbi.get_name_translator(sonicList)
    return name2taxid

def countSpecies(r):
    '''Function to count the number of sonication species detected by culture'''
    sonicList=list(r[['sonication_culture_species1',
        'sonication_culture_species2',
        'sonication_culture_species3']])#,
#        'tissue_culture_species1',
#        'tissue_culture_species2',
#        'tissue_culture_species3',
#        'tissue_culture_species4',
#        'tissue_culture_species5']])

    ignores=['negative',None]
    sonicList=[s for s in sonicList if s not in ignores]
    sonicList=[s for s in sonicList if not pd.isnull(s)]

    return len(set(sonicList))

def name2taxidfunc(t):
    try:
        return int(name2taxid[t][0])
    except:
        return 0 

def sonicCompare(r):
    '''Function to compare molecular species to species detected by sonication culture or tissue culture'''
    sonicList=list(r[['sonication_culture_species1',
        'sonication_culture_species2',
        'sonication_culture_species3',
        'tissue_culture_species1',
        'tissue_culture_species2',
        'tissue_culture_species3',
        'tissue_culture_species4',
        'tissue_culture_species5']])

    if r['Taxid'] in sonicList:
        match = True
    else:
        match = False


    ignores=['negative',None]
    sonicList=[s for s in sonicList if s not in ignores]
    sonicList=[s for s in sonicList if not pd.isnull(s)]

    # check for weird long species names
    if len(r['Taxid'].split(' '))>2:
        sp=' '.join(r['Taxid'].split(' ')[0:2])
        if sp in sonicList:
            match = True

    # if genus only, give matches
    sonicGenus=[]
    for species in sonicList:
        s=species.split(' ')
        if s[-1]=='sp':
            if r['Taxid'].split(' ')[0]==s[0]:
                match = True
        if species=='coagulase negative Staphylococcus':
            if r['Taxid'].split(' ')[0] == 'Staphylococcus' and r['Taxid'].split(' ')[1] != 'aureus':
                match = True

    return match

################## read in meta data from Tree Street ###############################
meta=pd.read_excel('../lab_data/PJI sequencing culture summary.xlsx')
meta['barcode']='barcode' + meta['barcode'].map(lambda x: f'{x:0>2}') #.str.zfill(2)
print('Number of individual barcoded sequences',len(meta))

# count number of species per sample
meta['Number of True species']=meta.apply(countSpecies,axis=1)
meta['Number of True species']=np.where(meta['sonication_culture_species1']=='negative',0,meta['Number of True species'])
# drop a few samples that failed or were repeated
#RPB004_57	barcode08
#RPB004_57	barcode04
dropruns=[('RPB004_57', 'barcode08'),('RPB004_57','barcode04')]
meta=meta[~meta[['run_ID','barcode']].apply(tuple, axis=1).isin(dropruns)]

# add taxids from names 
meta['Sonication taxids']=meta.apply(sonicTaxons,axis=1)
meta['all culture taxids']=meta.apply(cultureTaxons,axis=1)

############# reduce to just saponin 5% treated and sonication fluid ##########
smeta=meta[meta['sample_type']=='sonication fluid']
smeta=smeta[smeta['treatment']=='saponin 5%']
print('Number of sonication saponin barcoded sequences',len(smeta))
# lab controls
dflc=meta[meta['sample_type']=='negative']
dflc=dflc[dflc['sample_ID']!='Cooked Meat Media']
smeta=pd.concat([smeta,dflc])

snmeta=smeta[smeta['sonication_culture_species1']=='negative']
print('Number of sonication saponin barcoded sequences negative',len(snmeta))
snmeta=smeta[smeta['tissue_culture_species1']=='negative']
print('Number of sonication saponin barcoded sequences sonication and tissue culture negative',len(snmeta))
enmeta=meta[meta['sample_type']=='negative']
enmeta=enmeta[enmeta['treatment']=='saponin 5%']
print('Number of sonication saponin barcoded extraction sequences negative',len(enmeta))
#smeta=pd.concat([enmeta,smeta])



######################## read in domains file data from nanopore ######################
domains=pd.read_csv('../crumpit_out/all_domains.csv',sep='\t')
domains['run_ID']=domains['sample_ID'].map(removeRedo)
domains['Bacterial gigabases']=domains['bacterial bases']/1000000000
domains['Human gigabases']=domains['human bases']/1000000000
domains['Viral gigabases']=domains['viral bases']/1000000000
domains['Proportion human']=domains['human bases']/domains['total bases']

################# add in original domains file becuase reruns had had human bases removes #####
domainsO=pd.read_csv('../crumpit_out/all_domains_original.csv',sep='\t')
domainsO['barcode']=domainsO['barcode'].map(renameBarcode)
domainsO['run_ID']=domainsO['sample_ID'].map(removeRedo)
domainsO['Bacterial gigabases']=domainsO['bacterial bases']/1000000000
domainsO['Human gigabases']=domainsO['human bases']/1000000000
domainsO['Viral gigabases']=domainsO['viral bases']/1000000000
domainsO['Proportion human']=domainsO['human bases']/domainsO['total bases']

domains=domains.merge(domainsO, on=['run_ID','barcode'],how='left',suffixes=('','_porechop'))

################## species reads and bases ########################################
species=pd.read_csv('../crumpit_out/all_species.csv')
species['run_ID']=species['Run_name'].map(removeRedo)

################## mapping results ###############################################
maps=pd.read_csv('../crumpit_out/all_map.csv')
maps['run_ID']=maps['sample'].map(removeRedo)

########## add caprae reruns that include caprae as a species ####################
cpruns=[('RPB004_2','barcode04'),
        ('RPB004_10','barcode02'),
        ('RPB004_54','barcode07'),
        ('RPB004_63','barcode11')]
cmaps=pd.read_csv('../crumpit_out/caprae_all_map.csv')
cmaps['run_ID']=cmaps['sample'].map(removeRedo)
cmaps=cmaps[cmaps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=maps[~maps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=pd.concat([maps,cmaps]) # add new runs

#################### merge all dataframes to one ########################################
df=maps.merge(domains,on=['run_ID','barcode'],how='left')
df=df.merge(species,left_on=['run_ID','barcode','Species name'],
        right_on=['run_ID','Barcode','Taxid'],how='left')
df=df.merge(meta,on=['run_ID','barcode'],how='left')
domains=domains.merge(meta,on=['run_ID','barcode'],how='left')

##################### remove plasmids and any species with less than 10 reads ############
## this makes the df a lot smaller!
df=df[df['barcode']!='unclassified']
df['chroms']=df.groupby(['run_ID','Barcode','Taxid'])['chrom'].transform('count')
df['chrom max']=df.groupby(['run_ID','Barcode','Taxid'])['len'].transform(max)
df['chrom longest']=np.where(df['len']==df['chrom max'], True, False)
df=df[df['chrom longest']==True]
df=df[df['Reads']>10]

#################### add taxids from species names and remove all non bacteria ###############################
descendants = ncbi.get_descendant_taxa(2,collapse_subspecies=False,intermediate_nodes=True)
name2taxid = ncbi.get_name_translator(df['Taxid'].unique())
df['Taxonomy ID']=df['Taxid'].map(name2taxidfunc)
df['Taxonomy ID']=df['Taxonomy ID'].map(int)
df=df[df['Taxonomy ID'].isin(descendants)]

# compare
df['sonic match']=df.apply(sonicCompare, axis=1)
#print(df)

# add some useful columns
df['prop bac bases sample']=df['Bases']/df['bacterial bases']
df['mapped bases']=df['avCov']*df['x1']
df['prop mapped']=df['mapped bases']/df['Bases']
df['mapped spread']=df['x1']/df['Bases']
df.to_csv('dat.csv')

###################### initial stats ######################################################

#################### functions #############################################

## create two dictionary to be used multiple times instead of calling ete3
sonic_descendants={}
for d in df['Sonication taxids']:
    if pd.isna(d): continue
    for s in d:
        if s not in sonic_descendants:
            sonic_descendants[s]=ncbi.get_descendant_taxa(d[s][0],intermediate_nodes=True)
            sonic_descendants[s].append(d[s][0])

def FNsonic(r):
# add False Negative counts for sonication fluids culture
    try:
        species2taxid2=ncbi.get_name_translator(r['Detected species'])
        taxids=[species2taxid2[s][0] for s in species2taxid2]
    except:
        taxids=[]

    found=0
    TPs,FNs=[],[]
    if r['Number of True species']>0:
        for s in r['Sonication taxids']:
            if any([t in taxids for t in sonic_descendants[s]])==True:
                found+=1
                TPs.append(s)
            else:
                FNs.append(s)
    FNs=[x for x in FNs if x not in TPs]
    r['False negative species']=FNs
    r['False Negatives']=r['Number of True species']-found 
    return r

def reduceEntero(r):
# reduce multi Enterococcus species
    if 'Enterobacter' not in r['Sonication taxids']:
        return r
    if type(r['Detected species']) == float:
        return r
    else:
        genuses=[] #[s.split(' ')[0] for s in r['species list']]
        for s in r['Detected species']:
            try:
                genuses.append(s.split(' ')[0])
            except:
                pass
        n=genuses.count('Enterobacter')
        if n>1:
            r['True positive']=(r['True positive']-n)+1
            return r
        else:
            return r


## create two dictionary to be used multiple times instead of calling ete3
descendants={}
for d in df['all culture taxids']:
    if pd.isna(d): continue
    for s in d:
        if d[s][0] not in descendants:
            descendants[d[s][0]]=ncbi.get_descendant_taxa(d[s][0],intermediate_nodes=True)

print(descendants)
species2taxid=ncbi.get_name_translator(df['Species name'])

def FPlist(r):
# creat column of False positive species
    ''' return a list of species that weren't in any of the cultures '''

    # get a list of taxids that could be in culture (and descendants)
    d=r['all culture taxids']
    cultures=[]
    for s in d:
        descendant=descendants[d[s][0]]
        descendant.append(d[s][0])
        cultures.extend(descendants)
    r['Culture descendants']=cultures

    # check species against list
    FP,TP=[],[]
    if type(r['Detected species'])==list:
        for species in r['Detected species']:
            if species2taxid[species][0] in cultures:
                TP.append(species)
            else:
                FP.append(species)
    r['False species']=FP
    r['True species']=TP

    return r

##################################### main stats function ##################################
def stats(d,df=None,meta=None, sampleType='sonication fluid', treatment='saponin 5%'):
    covcut=d['coverage']
    bacbases=d['prop bases']
    propmapped=d['map prop']
    minreads=d['minimum reads']
    runNumbers=df.groupby(['run_ID','barcode'])['Species name'].count()
    #runNumbers.to_excel('runNumbers.xlsx')
    totalSamples=len(runNumbers)
    print('Total samples',totalSamples)

    # lab controls
    dflc=df[df['sample_type']=='negative']
    dflc=dflc[dflc['sample_ID_x']!='Cooked Meat Media']

    # just sonication samples with saponin at 5%
    if sampleType != None:
        df=df[df['sample_type']==sampleType]
    if treatment != None:
        df=df[df['treatment']==treatment]

    #if sampleType != None:
    df=pd.concat([df,dflc])
    #runNumbers=df.groupby(['run_ID','barcode'])['Species name'].count()
    #runNumbers.to_excel('runNumbersSonic.xlsx')
    totalSamples=len(runNumbers)
    print('Total {0} {1} samples'.format(sampleType, treatment),totalSamples)

    # filter out all samples with low genome coverage breadth
    dfU=df[df['covBread_1x_percent']<covcut]
    df=df[df['covBread_1x_percent']>=covcut]
    runNumbers=df.groupby(['run_ID','barcode'])['Species name'].count()
    totalSamples=len(runNumbers)
    print('Total {0} {1} samples above {2}% coverage'.format(sampleType, treatment, covcut),totalSamples)

    # find low abundance samples and species
    undercov=dfU[['run_ID','barcode']].apply(tuple, axis=1).to_list()
    dfU=dfU[dfU[['run_ID','barcode']].apply(tuple, axis=1).isin(undercov)]
    dfU=dfU[dfU['Reads']>=minreads]
    dfU=dfU[dfU['prop bac bases sample']>=bacbases]
    dfU=dfU[dfU['prop mapped']>=propmapped]

    # combine back to df
    df=pd.concat([df,dfU])

    # get list of passed species
    specieslist=df.groupby(['run_ID','barcode'])['Species name'].apply(list)
    specieslist=specieslist.reset_index()
    specieslist.rename(columns={'Species name':'Detected species'},inplace=True)

    # count number of species that match sonication/tissue culture results
    g=df.groupby(['run_ID','barcode','sonic match'])['Species name'].count()
    g=g.reset_index()
    g=g.pivot_table(index=['run_ID','barcode'],columns=['sonic match'],values=['Species name'])

    #merge meta with true/false species counts
    meta=meta.merge(g,on=['run_ID','barcode'],how='left')
    meta.rename(columns={('Species name', True):'True positive',
        ('Species name', False):'Detected False'},
        inplace=True)
    meta['True negative']=np.where(meta['Detected False']>0,0,1)
    meta['False positive']=np.where(meta['Detected False']>0,1,0)

    # merge with species list
    meta=meta.merge(specieslist,on=['run_ID','barcode'],how='left')

    # add False Negative counts for sonication fluids culture
    meta=meta.apply(FNsonic, axis=1)
#    meta['False Negatives']=meta.apply(FNsonic, axis=1)

    # creat column of False positive species
    meta=meta.apply(FPlist,axis=1)
    # reduce multi Enterococcus species
    meta=meta.apply(reduceEntero,axis=1)

    # write results
    headers=['sample_ID', 'patient', 'collection_date', 'sample_type',
       'treatment', 'run_ID', 'barcode', 'sonication_culture_species1',
       'sonication_quantification_1', 'sonication_culture_species2',
       'sonication_quantification_2', 'sonication_culture_species3',
       'sonication_quantification_3', 'tissue_culture_species1',
       'tissue_quantification_1', 'tissue_culture_species2',
       'tissue_quantification_2', 'tissue_culture_species3',
       'tissue_quantification_3', 'tissue_culture_species4',
       'tissue_quantification_4', 'tissue_culture_species5',
       'tissue_quantification_5', 'Synovialfluid_culture_species1',
       'Synovialfluid_culture_species2', 'pus_culture_species1', 'notes',
       'Number of True species', 'Sonication taxids', 'all culture taxids',
       'Detected False', 'True positive', 'True negative', 'False positive','False Negatives',
       'Detected species', 'False species', 'True species','False negative species']
    meta=meta[headers]
    if sampleType:
        sampleType=sampleType.replace(' ','-')
    if treatment:
        treatment=treatment.replace(' ','-')
    meta.to_csv('results/stats_cov{0}_propbases{1}_propmapped{2}_{3}_{4}_{5}.csv'.format(covcut,
        bacbases,
        propmapped,
        sampleType, 
        treatment,
        minreads))

    # make list of false positives
    FP=meta['False species'].sum()
    #FP=[i for i in s for s in FP]
    FP=set(FP)
    #with open('FP_species_{0}_{1}.txt'.format(sampleType, treatment),'wt') as outf:
    #    outf.write('\n'.join(FP))

    # calculate sensistivity and specificity
    expectedSpecies=sum(meta['Number of True species'])
    detectedSpecies=meta['True positive'].sum()
    falseSpecies=meta['Detected False'].sum()
    sensitivity=detectedSpecies/expectedSpecies
    #print('{3} {4} Sensitivity: {0}% ({1}/{2})'.format(sensitivity,detectedSpecies,expectedSpecies,sampleType, treatment) )
    TN=meta['True negative'].sum()
    FP=meta['Detected False'].sum()
    specificity=TN/(TN+FP)
    #print('{3} {4} Specificity: {0}% ({1}/{1}+{2})'.format(specificity,TN,FP, sampleType, treatment)) 
    d['TPR']=sensitivity
    d['FPR']=specificity
    return d
#    return meta,sensitivity,specificity


def covRange(df,smeta):
#    st,tpr,fpr=stats(df,smeta,99)
    l=[]
    for r in range(10,60,5):
        for b in [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
            for m in [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
                for mr in range(0,1000,100):
                #st,tpr,fpr=stats(df,smeta,r,b,m)
                    l.append({'coverage':r,
                        'prop bases':b,
                        'map prop':m,
                        'minimum reads':mr})
    p=Pool(4)
    l2=p.map(partial(stats,df=df,meta=smeta), l)
    roc=pd.DataFrame(l2)
    roc.to_csv('roc_multiThreaded.csv')
    return roc

OptimumThreshold={'coverage':50,
        'prop bases':0.6,
        'map prop':0.8,
        'minimum reads':700}
l2=stats(OptimumThreshold,df=df,meta=smeta)
print(l2)
l3=stats(OptimumThreshold,df=df,meta=meta, sampleType=None, treatment=None)
print(l3)
roc=covRange(df,smeta)
roc=pd.read_csv('roc_multiThreaded.csv')
################################# effect of saponin ####################################
def saponinEffectAll(df,domains):
    conditions=[('sonication fluid','filter 5uM'),('sonication fluid','saponin 5%'),
            ('sonication fluid','saponin 1%'),('sonication fluid','saponin 0.1%')]
    df=df[df[['sample_type','treatment']].apply(tuple, axis=1).isin(conditions)]
    df['sample count']=df.groupby(['sample_ID'])['treatment'].transform('count')
#    df=df[df['sample count']==2]
    # drop that one that failed
    df=df[~df['sample_ID'].isin(['F52352'])]
    df=df.merge(domains,how='left',on=['run_ID','barcode'])
    df=df[df['sonication_culture_species1_x']!='negative']
    #ax=sns.pointplot(x='treatment_x',y='Proportion human_porechop',hue='sample_ID',data=df,ci="sd")

    # plot proportion of human bases by treatment
    ax=sns.boxplot(x='treatment_x',y='Proportion human_porechop',data=df, showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='Proportion human_porechop',data=df,color=".25")
    plt.xlabel("Human depletion method")
    plt.ylabel("Proportion of human bases sequenced")
    plt.savefig('figs/saponin_treatment_all.svg')
    plt.savefig('figs/saponin_treatment_all.png')
    plt.savefig('figs/saponin_treatment_all.pdf')
#    plt.show()

    ## plot total bases by treatment
    ax=sns.boxplot(x='treatment_x',y='total bases_porechop',data=df,showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='total bases_porechop',data=df,color=".25")
    plt.xlabel("Human depletion method")
    plt.ylabel("Total bases sequenced")
    plt.savefig('figs/saponin_treatment_total_all.svg')
    plt.savefig('figs/saponin_treatment_total_all.png')
    plt.savefig('figs/saponin_treatment_total_all.pdf')
    plt.clf()
#    plt.show()

    ## plot bacterial bases by treatment
    ax=sns.boxplot(x='treatment_x',y='bacterial bases_porechop',data=df,showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='bacterial bases_porechop',data=df,color=".25")
    plt.xlabel("Human depletion method")
    plt.ylabel("Bacterial bases sequenced")
    plt.savefig('figs/saponin_treatment_bacteria_all.svg')
    plt.savefig('figs/saponin_treatment_bacteria_all.png')
    plt.savefig('figs/saponin_treatment_bacteria_all.pdf')
    plt.clf()
#    plt.show()

    def diffConc(p):
        dfSap=df[df['treatment_x']=='saponin {}%'.format(p)]
        dfFil=df[df['treatment_x']=='filter 5uM']
    
    
        dfSred=dfSap[['sample_ID','Proportion human_porechop']]
        dfFilred=dfFil[['sample_ID','Proportion human_porechop']]
        dfSred.rename(columns={'Proportion human_porechop': 'Saponin human proportion'},inplace=True)
        dfFilred.rename(columns={'Proportion human_porechop': 'Filter human proportion'},inplace=True)
        df2=dfSred.merge(dfFilred,on='sample_ID',how='left')
        df2=df2.dropna()
        df2['Saponin reduction']=df2['Filter human proportion']-df2['Saponin human proportion']
        df2['Saponin effect']=(df2['Filter human proportion']/df2['Saponin reduction'])*100
        df2['% of original human prop']=(df2['Saponin human proportion']/df2['Filter human proportion'])*100
        df2['% reduction in human proportion of bases']=100-df2['% of original human prop']
        df2=df2[df2['Saponin effect']!=np.inf]
        print('saponin effect at {}%'.format(p))
        print(df2['% reduction in human proportion of bases'].describe())
        df2.to_csv('saponin_reduction_{}.csv'.format(p))
    #    ax3=sns.distplot(df2['% reduction in human proportion of bases'])
    #    plt.show()
    
    #    ## paired T-test
        from scipy import stats
        ids=df2['sample_ID'].to_list()
        dfSap=dfSap[dfSap['sample_ID'].isin(ids)]
        dfFil=dfFil[dfFil['sample_ID'].isin(ids)]
        t,P=stats.ttest_rel(dfSap['Proportion human_porechop'],dfFil['Proportion human_porechop'])
        print('Saponin {} treatment T statistic:'.format(p),t)
        print('Saponin {} treatment P value:'.format(p),P)
    #
    #    ## median, IQR for proportion of human bases
        print('saponin effect at {}%'.format(p))
        print(dfSap[['Proportion human_porechop']].describe())
        print('saponin effect at {}%'.format(p))
        print(dfFil[['Proportion human_porechop']].describe())
    for p in [5,1,0.1]:
        diffConc(p)

#saponinEffectAll(meta,domains)

def saponinEffect(df,domains):
    conditions=[('sonication fluid','filter 5uM'),('sonication fluid','saponin 5%')]
    df=df[df[['sample_type','treatment']].apply(tuple, axis=1).isin(conditions)]
    df['sample count']=df.groupby(['sample_ID'])['treatment'].transform('count')
    df=df[df['sample count']==2]
    # drop that one that failed
    df=df[~df['sample_ID'].isin(['F52352'])]
    df=df.merge(domains,how='left',on=['run_ID','barcode'])
    df=df[df['sonication_culture_species1_x']!='negative']
    df['Sample']=df['patient_x'].map(int)
    #ax=sns.pointplot(x='treatment_x',y='Proportion human_porechop',hue='sample_ID',data=df,ci="sd")

    # rename to add mu 
    def addMu(r):
        return r.replace('u','\u03BC')

    df['treatment_x']=df['treatment_x'].map(addMu)


    # plot proportion of human bases by treatment
    df['Percentage of bases classified as human']=df['Proportion human_porechop']*100
    ax=sns.boxplot(x='treatment_x',y='Percentage of bases classified as human',data=df, showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='Percentage of bases classified as human',data=df,color=".25")
    plt.xlabel("Human depletion method")
    #plt.ylabel("Proportion of bases classified as human")
    plt.savefig('figs/saponin_treatment.svg')
    plt.savefig('figs/saponin_treatment.png')
    plt.savefig('figs/saponin_treatment.pdf')
#    plt.show()

    ## plot total bases by treatment
    ax=sns.boxplot(x='treatment_x',y='total bases_porechop',data=df,showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='total bases_porechop',data=df,color=".25")
    plt.xlabel("Human depletion method")
    plt.ylabel("Total bases sequenced")
    plt.savefig('figs/saponin_treatment_total.svg')
    plt.savefig('figs/saponin_treatment_total.png')
    plt.savefig('figs/saponin_treatment_total.pdf')
    plt.clf()
#    plt.show()

    ## plot bacterial bases by treatment
    ax=sns.boxplot(x='treatment_x',y='bacterial bases_porechop',data=df,showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='bacterial bases_porechop',data=df,color=".25")
    ax.set_yscale('log')
    plt.xlabel("Human depletion method")
    plt.ylabel("Bacterial bases sequenced")
    plt.savefig('figs/saponin_treatment_bacteria.svg')
    plt.savefig('figs/saponin_treatment_bacteria.png')
    plt.savefig('figs/saponin_treatment_bacteria.pdf')
    plt.clf()

    ## plot bacterial proportion by treatment
    df['proportion bacterial bases_porechop']=df['bacterial bases']/df['total bases_porechop']
    ax=sns.boxplot(x='treatment_x',y='proportion bacterial bases_porechop',data=df,showfliers=False)
    ax=sns.swarmplot(x='treatment_x',y='proportion bacterial bases_porechop',data=df,color=".25")
    plt.xlabel("Human depletion method")
    plt.ylabel("Proportion of bases classified as bacteria")
    plt.savefig('figs/saponin_treatment_prop_bacteria.svg')
    plt.savefig('figs/saponin_treatment_prop_bacteria.png')
    plt.savefig('figs/saponin_treatment_prop_bacteria.pdf')
    plt.clf()
#    plt.show()

#    ## plot bacterial coverage by treatment
#    ax=sns.boxplot(x='treatment_x',y='bacterial bases_porechop',data=df,showfliers=False)
#    ax=sns.swarmplot(x='treatment_x',y='bacterial bases_porechop',data=df,color=".25")
#    plt.xlabel("Human depletion method")
#    plt.ylabel("Bacterial bases sequenced")
#    plt.savefig('figs/saponin_treatment_bacteria.svg')
#    plt.savefig('figs/saponin_treatment_bacteria.png')
#    plt.savefig('figs/saponin_treatment_bacteria.pdf')
#    plt.clf()

    dfSap=df[df['treatment_x']=='saponin 5%']
    dfFil=df[df['treatment_x']=='filter 5\u03BCM']

    print('XXXXXXXXXXX prob bacterial bases XXXXXXXXXXXX')
    print('Sap')
    print(dfSap['proportion bacterial bases_porechop'].describe())
    print(dfSap['bacterial bases_porechop'].describe())
    print('Fil')
    print(dfFil['proportion bacterial bases_porechop'].describe())
    print(dfFil['bacterial bases_porechop'].describe())
    print('XXXXXXXXXXX prob bacterial bases END XXXXXXXXXXXX')

    dfSred=dfSap[['Sample','Proportion human_porechop']]
    dfFilred=dfFil[['Sample','Proportion human_porechop']]
    dfSred.rename(columns={'Proportion human_porechop': 'Saponin human proportion'},inplace=True)
    dfFilred.rename(columns={'Proportion human_porechop': 'Filter human proportion'},inplace=True)
    df2=dfSred.merge(dfFilred,on='Sample',how='left')
    df2['Saponin reduction']=df2['Filter human proportion']-df2['Saponin human proportion']
    df2['Saponin effect']=(df2['Filter human proportion']/df2['Saponin reduction'])*100
    df2['% of original human prop']=(df2['Saponin human proportion']/df2['Filter human proportion'])*100
    df2['% reduction in human proportion of bases']=100-df2['% of original human prop']
    df2=df2[df2['Saponin effect']!=np.inf]
    print(df2['Saponin effect'].describe())
    df2.to_csv('saponin_reduction.csv')
#    ax3=sns.distplot(df2['% reduction in human proportion of bases'])
#    plt.show()

    ## paired T-test
    from scipy import stats
    t,p=stats.ttest_rel(dfSap['Proportion human_porechop'].to_list(),dfFil['Proportion human_porechop'])
    print('Saponin treatment T statistic:',t)
    print('Saponin treatment P value:',p)

    ## median, IQR for proportion of human bases
    print(dfSap[['Proportion human_porechop']].describe())
    print(dfFil[['Proportion human_porechop']].describe())

    df.to_csv('saponin_effects.csv',index=False)

saponinEffect(meta,domains)


############################### plots ##################################################

def plotcovROC(df):
    df['1-FPR']=1-df['FPR']
    g=sns.lineplot(x='1-FPR',y='TPR',data=df,estimator=None)
    g.plot([0,1], [0,1], '-k')
    plt.savefig('figs/cutoff_ROC.svg')
    plt.savefig('figs/cutoff_ROC.pdf')
    plt.show()

#plotcovROC(roc)

def plotYouden(df):
    df['Youden Index']=df['TPR']+df['FPR']-1
    g=sns.lineplot(x='coverage',y='Youden Index',data=df,estimator=None)
    plt.savefig('figs/cutoff_Youden_Index.svg')
    plt.savefig('figs/cutoff_Youden_Index.pdf')

def plotYoudenHeatmap(df):
    df['Youden Index']=df['TPR']+df['FPR']-1
    df2=df[['prop bases','map prop','Youden Index']]
    df3=df2.pivot(index='prop bases',columns='map prop',values='Youden Index')
    g=sns.heatmap(df3)
    plt.savefig('figs/Youden_heatmap.svg')
    plt.savefig('figs/Youden_heatmap.pdf')



#plotYouden(roc)

def plotLowSens(df,domains):
    df=df.merge(domains, on=['run_ID','barcode'], how='left')
    df['Low sensitivity']=np.where(df['False Negatives']>0,True,False)
    df['Bacterial gigabases']=df['bacterial bases']/1000000000
    df['Human gigabases']=df['human bases']/1000000000
    df['Viral gigabases']=df['viral bases']/1000000000
    df['Total gigabases']=df['total bases']/1000000000

#    g=sns.boxplot(x='Low sensitivity',y='Proportion human', data=df)
#    plt.show()
#    plt.clf()
#    g=sns.catplot(x='Low sensitivity',y='Bacterial gigabases', data=df)
#    plt.show()
#    plt.clf()
#    g=sns.boxplot(x='Low sensitivity',y='Total gigabases', data=df)
#    g=sns.catplot(x='Low sensitivity',y='Total gigabases', data=df)
#    plt.show()
    g=sns.scatterplot(x='Bacterial gigabases',y='Total gigabases',hue='Low sensitivity',data=df)
    plt.show()

#plotLowSens(st,domains)


def plotDomains(df):
    ''' plot proportion of human bases compared to others'''
    df=df[df['sample_type']=='sonication fluid']
    df['Bacterial gigabases']=df['bacterial bases']/1000000000
    df['Human gigabases']=df['human bases']/1000000000
    df['Viral gigabases']=df['viral bases']/1000000000
    ax=df[['sample_ID_y','Bacterial gigabases','Human gigabases_porechop','Viral gigabases']].plot(kind='barh', stacked=True, x='sample_ID_y')
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10),
                              ncol=3, fancybox=True, shadow=False)
    plt.tight_layout()
    plt.xlabel("Gigabases")
    plt.savefig('figs/domains_stacked_sonication.svg',format="svg")
    plt.clf()

#plotDomains(domains)
def runNum(r):
    return int(r.replace('RPB004_',''))

def plotRuns(df):
    ''' plot proportion of human bases compared to others'''
    plt.clf()
    df['Gigabases']=df['total bases_porechop']/1000000000
    df['Run number']=df['run_ID'].map(runNum)
    df['Run gigabases']=df.groupby('run_ID')['Gigabases'].transform(sum)
    df2=df[['Run number','Run gigabases']]
    df2.drop_duplicates(inplace=True)
#    df2.sort_values(by='Run gigabases',inplace=True)
    df2.sort_values(by='Run number',inplace=True,ascending=False)

    ax=df2.plot(kind='barh',x='Run number')
    print('XXXXXXX RUN GIGABASES XXXXXXXXX\n')
    print(df2['Run gigabases'].describe())
    print('XXXXXXX RUN GIGABASES END XXXXXXXXX')
    plt.tight_layout()
    plt.xlabel("Gigabases")
#    plt.savefig('figs/total_run_bases.svg',format="svg")
#    plt.savefig('figs/total_run_bases.png',format="png")
#    plt.savefig('figs/total_run_bases.pdf',format="pdf")
    plt.show()
    plt.clf()

#plotRuns(domains)
#domains.to_csv('domains.csv')
def plotRelativeHuman(df):
    ''' plot relative human concentration between treatments '''
    df=df[df['treatment'].isin(['saponin 5%','filter 5uM'])]
    g=sns.violinplot(y='treatment',x='Proportion human', data=df)
#    g=sns.catplot(y='treatment',x='Proportion human', data=df)
    plt.tight_layout()
    plt.savefig('figs/sonication_treatment_human_prop.svg',format="svg")
    plt.clf()

#plotRelativeHuman(domains)

def plotCoverage(df):
    df=df[df['sample_type']=='sonication fluid']
    df=df[df['treatment']=='saponin 5%']
    g=sns.catplot(x='sonic match',y='covBread_1x_percent', data=df)
    plt.show()

#plotCoverage(df)







