#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ast
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')

df=pd.read_csv('results/stats_cov45_propbases0.6_propmapped0.4_None_None.csv')

def removeRedo(r):
    return r.replace('redo_','').replace('PJI_','').replace('carpae_','')
def renameBarcode(r):
    return r.replace('BC','barcode')

sp=pd.read_csv('../crumpit_out/all_species.csv')
sp['run_ID']=sp['Run_name'].map(removeRedo)

######################## read in domains file data from nanopore ######################
domains=pd.read_csv('../crumpit_out/all_domains.csv',sep='\t')
domains['run_ID']=domains['sample_ID'].map(removeRedo)
domains['Bacterial gigabases']=domains['bacterial bases']/1000000000
domains['Human gigabases']=domains['human bases']/1000000000
domains['Viral gigabases']=domains['viral bases']/1000000000
domains['Proportion human']=domains['human bases']/domains['total bases']

################# add in original domains file becuase reruns had had human bases removes #####
domainsO=pd.read_csv('../crumpit_out/all_domains_original.csv',sep='\t')
domainsO['barcode']=domainsO['barcode'].map(renameBarcode)
domainsO['run_ID']=domainsO['sample_ID'].map(removeRedo)
domainsO['Bacterial gigabases']=domainsO['bacterial bases']/1000000000
domainsO['Human gigabases']=domainsO['human bases']/1000000000
domainsO['Viral gigabases']=domainsO['viral bases']/1000000000
domainsO['Proportion human']=domainsO['human bases']/domainsO['total bases']

domains=domains.merge(domainsO, on=['run_ID','barcode'],how='left',suffixes=('','_porechop'))
################## mapping results ###############################################
maps=pd.read_csv('../crumpit_out/all_map.csv')
maps['run_ID']=maps['sample'].map(removeRedo)

########## add caprae reruns that include caprae as a species ####################
cpruns=[('RPB004_2','barcode04'),
        ('RPB004_10','barcode02'),
        ('RPB004_54','barcode07'),
        ('RPB004_63','barcode11')]
cmaps=pd.read_csv('../crumpit_out/caprae_all_map.csv')
cmaps['run_ID']=cmaps['sample'].map(removeRedo)
cmaps=cmaps[cmaps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=maps[~maps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=pd.concat([maps,cmaps]) # add new runs
maps['chroms']=maps.groupby(['run_ID','barcode','Species name'])['chrom'].transform('count')
maps['chrom max']=maps.groupby(['run_ID','barcode','Species name'])['len'].transform(max)
maps['chrom longest']=np.where(maps['len']==maps['chrom max'], True, False)
maps=maps[maps['chrom longest']==True]


# meta
meta=df[['sample_ID', 'patient', 'sample_type', 'treatment', 'run_ID','barcode','Number of True species']]

dfs=[]
def getReads(r):
    species=r['Detected species']
    falseSpecies=r['False species']
    if pd.isnull(species) == True:
        return None
    species=ast.literal_eval(species)
    fp=ast.literal_eval(falseSpecies)
    for s in species:
        sp2=sp.loc[(sp['run_ID']==r['run_ID']) & 
                (sp['Barcode']==r['barcode']) &
                (sp['Taxid']== s)]
        if s in fp:
            sp2['Species lab verified']=False
        else:
            sp2['Species lab verified']=True

        dfs.append(sp2)

df.apply(getReads,axis=1)
df2=pd.concat(dfs)

df2=df2.merge(domains,left_on=['run_ID','Barcode'], 
        right_on=['run_ID','barcode'],how='left')
df2=df2.merge(maps,left_on=['run_ID','barcode','Taxid'],
        right_on=['run_ID','barcode','Species name'],
        how='left')

df=df2.merge(meta, on=['run_ID','barcode'],how='left')
df.to_csv('/tmp/data.csv')

df['low abundance']=np.where(df.covBread_1x_percent>=45, False,True)
df=df[df['sample_type'].isin(['sonication fluid', 'negative'])]
df=df[df['treatment'].isin(['saponin 5%','filter 5uM'])]
df['FP in -ve']=np.where((df['Number of True species']==0) & (df['Species lab verified']==False), True,False)

df2=df[df['FP in -ve']==True]
print(df2.columns)
print(df2[['patient','sample','sample_type','covBread_1x_percent', 'Taxid','low abundance','Species lab verified']])

#ax=sns.swarmplot(x='FP in -ve',y='Bases',
#        data=df,
#        hue='low abundance',
#        #hue='Species lab verified'
#        )
#ax.set_yscale("log")
#plt.show()
#plt.clf()
#df2=df[df['FP in -ve']==False]
df2=df[df['low abundance']==True]
print(df2[['patient','sample_type','treatment','covBread_1x_percent', 'Taxid','low abundance','Species lab verified','Reads']])

#ax=sns.swarmplot(x='FP in -ve',y='Bases',
#        data=df,
#        #hue='',
#        hue='Species lab verified'
#        )
#ax.set_yscale("log")
#plt.show()
#plt.clf()

ax=sns.scatterplot(x='Reads',y='Bases',
        data=df2,
        style='treatment',
        hue='Species lab verified'
        )

plt.savefig('figs/low_abundance_readsBases.png')
plt.show()
plt.clf()

