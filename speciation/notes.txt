[x] analysis script compiles meta data and crumpit reports and merges together
[x] currently comparing based on species name with some wrangling for 'sp' species and CoNS
[ ] potentially change to taxid system with LCA for improved sens/specs
[x] Multi enteroc species might be inflating sens when culture records only genenus
[x] need list of plausible anaerobes, increase sensitivity?
[ ] 	add list to discount step. Add to sensistivity or not? what have other done?
[x] decide if need threshold for low abundance species, i.e. when coverage <30% of genomes
[x]       - does run performance have an impact? Some sort of quality filtering based on bases and human depletion success
[x]       - starting to think a spike would be useful for removing "poor performing runs"
[x] no mapping from Staph caprae samples... added remaps but only 1 worked
[ ] filter out runs with <5cfu, so they don't affect sensisitvity stats, including these runs:
[ ]       RPB004_9	barcode07	Staphylococcus epidermidis	<5
[ ]       RPB004_54	barcode07	Coagulase negative Staphylococcus	<5
[ ]       RPB004_63	barcode11	Coagulase negative Staphylococcus	<5
[x] need to change domains file for pre rerun data to include human bases... but this doesn't include 12a barcodes

[ ] plots
[x] why we chose mapping cuttof?
[ ] 	mapping coverage/TP or FP catplot

