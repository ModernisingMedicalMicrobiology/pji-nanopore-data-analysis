#!/usr/bin/env python3
import pandas as pd

df=pd.read_csv('roc_multiThreaded.csv')
df['1-FPR']=1-df['FPR']
df['Youden Index']=df['TPR']+df['FPR']-1
#df.to_csv('cuttoff_conditions.csv',index=False)
#df=pd.read_csv('cuttoff_conditions.csv')

y=df['Youden Index'].max()
df=df[df['Youden Index']==y]

print('Matching Youden index')
print(df)

m=df['coverage'].min()
df=df[df['coverage']==m]

pb=df['prop bases'].min()
df=df[df['prop bases']==pb]

mp=df['map prop'].min()
df=df[df['map prop']==mp]

mr=df['minimum reads'].min()
df=df[df['minimum reads']==mr]

print('Lowest paramter settings')
print(df)
