# Speciation and saponin analysis

## Speciation 
To run the analysis script that calculates the ROC curve and Youden's index, plus figures for the papers

```bash
python3 analysis.py
```

## Saponin stats
Further statistics required for saponin effect can be run with 
```
python3 saponin_reduction_Wilcoxen.py
```
