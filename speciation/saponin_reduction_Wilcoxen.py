#!/usr/bin/env python3
import pandas as pd
from scipy.stats import wilcoxon

df=pd.read_csv('saponin_reduction.csv')
w,p=wilcoxon(df['Saponin human proportion'],df['Filter human proportion'])
print(w,p)
