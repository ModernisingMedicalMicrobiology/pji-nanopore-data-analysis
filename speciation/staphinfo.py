import pandas as pd
import numpy as np
from ast import literal_eval

df=pd.read_csv('stats.csv')
def fillList(r):
    if pd.isna(r['species list'])==True:
        return '[]'
    else:
        return r['species list']#.map(literal_eval)

df['species list']=df.apply(fillList,axis=1)
df['True species']=df['True species'].map(literal_eval)
df['species list']=df['species list'].map(literal_eval)
df['False species']=df['False species'].map(literal_eval)

def isitstaph(r):
    staph=False
    for i in r['True species']:
        if i.startswith('Staphyl'):
            staph=True
    for i in r['False species']:
        if i.startswith('Staphyl'):
            staph=True
    if type(r['species list'])==list:
        for i in r['species list']:
            if i.startswith('Staphyl'):
                staph=True
    return staph

df['Found staph']=df.apply(isitstaph,axis=1)
staphs=df[df['Found staph']== True]
print(staphs)

################## mapping results ###############################################
def removeRedo(r):
    return r.replace('redo_','').replace('PJI_','').replace('carpae_','')

maps=pd.read_csv('../all_map.csv')
maps['run_ID']=maps['sample'].map(removeRedo)

########## add caprae reruns that include caprae as a species ####################
cpruns=[('RPB004_2','barcode04'),
        ('RPB004_10','barcode02'),
        ('RPB004_54','barcode07'),
        ('RPB004_63','barcode11')]
cmaps=pd.read_csv('../caprae_all_map.csv')
cmaps['run_ID']=cmaps['sample'].map(removeRedo)
cmaps=cmaps[cmaps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=maps[~maps[['run_ID', 'barcode']].apply(tuple, axis=1).isin(cpruns)] # remove all non targetted reruns
maps=pd.concat([maps,cmaps]) # add new runs
maps['chrom max']=maps.groupby(['run_ID','barcode','Species name'])['len'].transform(max)
maps['chrom longest']=np.where(maps['len']==maps['chrom max'], True, False)
maps=maps[maps['chrom longest']==True]


ts=[item for sublist in staphs['True species'].to_list() for item in sublist]
fs=[item for sublist in staphs['False species'].to_list() for item in sublist]
staphsspecies=ts
staphsspecies.extend(fs)
maps=maps[maps['Species name'].isin(staphsspecies)]

sa=maps[maps['Species name']=='Staphylococcus aureus']


### merge dfs
df=staphs.merge(sa,on=['run_ID','barcode'],how='left')
#print(df)

## load micro/phoenix data
micro=pd.read_csv('../AMR/PSS2_StaphAMR_11_Micro.csv')
phonix=pd.read_csv('../AMR/PSS2_StaphAMR_11_Phoenix.csv') 
print(micro)

#df.to_csv('staphs.csv',index=False)
