import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df=pd.read_csv('roc_multiThreaded.csv')
df['1-FPR']=1-df['FPR']
df['Youden Index']=df['TPR']+df['FPR']-1
df.to_csv('cuttoff_conditions.csv',index=False)

covs=df['coverage'].unique()
mins=df['minimum reads'].unique()


def plotCovs(covs,df,mr):
    fig, axs = plt.subplots(2,5,sharex=True, sharey=True)
    for ax,cov in zip(axs.flatten(), covs):
        df2=df[df['coverage']==cov]
        df2=df2[df2['minimum reads']==mr]
        df2=df2[['prop bases','map prop','Youden Index']]
        df2=df2.pivot(index='prop bases',columns='map prop',values='Youden Index')
        if cov == covs[-1]:
            g=sns.heatmap(df2,vmin=0, vmax=1,ax=ax,cmap="YlGnBu")
        else:
            g=sns.heatmap(df2,vmin=0, vmax=1,ax=ax,cbar=False,cmap="YlGnBu")
        ax.set_title('{0}% coverage'.format(cov))
        ax.set_ylabel('')
        ax.set_xlabel('')
    fig.text(0.5, 0.04, 'Proportion of species bases that map', ha='center')
    fig.text(0.04, 0.5, 'Proportion of species bases in sample', va='center', rotation='vertical')
    plt.suptitle('Youden indexes with {0} minium reads'.format(mr))
    plt.show()
    plt.savefig('figs/heatmaps/Youden_heatmap_{0}_minReads.pdf'.format(mr))
    plt.savefig('figs/heatmaps/Youden_heatmap_{0}_minReads.svg'.format(mr))


#for mr in mins:
#    plotCovs(covs,df,mr)
plotCovs(covs,df,700)
